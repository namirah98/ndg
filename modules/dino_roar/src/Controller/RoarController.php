<?php
namespace Drupal\dino_roar\Controller;
use Symfony\Component\HttpFoundation\Response;
class RoarController
{
	// the controller should always (ok not always) return a Symfony response object
	public function roar(){
		return new Response('ROOOAR!');
	}
}