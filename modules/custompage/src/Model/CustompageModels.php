<?php 
/**
 * @file
 * Contains \Drupal\custompage\CustompageController.
 */

namespace Drupal\custompage\Model;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Url;
use \Drupal\custompage\Controller\HttpClient;

/**
 * Provides route responses for the Example module.
**/

class CustompageModels extends ControllerBase {
	
	static $username = 'idrmdigita';
	static $password = 'NamaKUj4t?';
	static $baseUrl = 'https://dolce-gusto.sahabatnestlerewards.co.id';

	public function index(){

	}
	public function accessToken(){
		return base64_encode(self::$username.':'.self::$password);
	}
	public function productRewardsCategoryListActive(){
		$url = self::$baseUrl.'/apidolcegusto/public/product_rewards_category/list_active';
		$request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Basic %s", self::accessToken())));
        $request->send();
        $output = json_decode($request->getHttpResponse());

        return $output;
	}
	public function userGetID($userID){
		$url = self::$baseUrl.'/apidolcegusto/public/user/get/'.$userID;
		$request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Basic %s", self::accessToken())));
        $request->send();
        $output = json_decode($request->getHttpResponse());

        return $output;
	}
	public function userInsert($userID, $data){
		$url = self::$baseUrl.'/apidolcegusto/public/user/insert';
		$request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Basic %s", self::accessToken())));
        $request->setPostData($data);
        $request->send();
        $output = json_decode($request->getHttpResponse());

        return $output;
	}

	public function productRewardsProduct($alias){
		$url = self::$baseUrl.'/apidolcegusto/public/product_rewards/product/'.$alias;
		$request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Basic %s", self::accessToken())));
        $request->send();
        $output = json_decode($request->getHttpResponse());

        return $output;
	}

	public function redeemList($data){
		$url = self::$baseUrl.'/apidolcegusto/public/redeem/list';
		$request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Basic %s", self::accessToken())));
        $request->setPostData($data);
        $request->send();
        $output = json_decode($request->getHttpResponse());

        return $output;
	}

	public function redeemAddressList($data){
		$url = self::$baseUrl.'/apidolcegusto/public/redeem/addressList';
		$request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Basic %s", self::accessToken())));
        $request->setPostData($data);
        $request->send();
        $output = json_decode($request->getHttpResponse());

        return $output;
	}

	public function redeemConfirmation($data){
		$url = self::$baseUrl.'/apidolcegusto/public/redeem/confirmation';
		$request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Basic %s", self::accessToken())));
        $request->setPostData($data);
        $request->send();
        $output = json_decode($request->getHttpResponse());

        return $output;
	}

	public function listDataCity(){
		$url = self::$baseUrl.'/apidolcegusto/public/list_data/city';
		$request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Basic %s", self::accessToken())));
        $request->send();
        $output = json_decode($request->getHttpResponse());

        return $output;
	}
	public function listDataProvince(){
		$url = self::$baseUrl.'/apidolcegusto/public/list_data/province';
		$request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Basic %s", self::accessToken())));
        $request->send();
        $output = json_decode($request->getHttpResponse());

        return $output;
	}
	public function redeemThankyou($data){
		$url = self::$baseUrl.'/apidolcegusto/public/redeem/thankyou';
		$request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Basic %s", self::accessToken())));
        $request->setPostData($data);
        $request->send();
        $output = json_decode($request->getHttpResponse());

        return $output;
	}
}