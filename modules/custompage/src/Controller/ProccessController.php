<?php
/**
 * @file
 * Contains \Drupal\custompage\CustompageController.
 */

namespace Drupal\custompage\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Url;
use \Symfony\Component\HttpFoundation\Response;
/**
 * Provides route responses for the Example module.
 */
class ProccessController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  private $authUsername;
  private $authPassword;
  private $baseUrl;

  public function __construct(){
    $this->authUsername = __AUTH_USERNAME__;
    $this->authPassword = __AUTH_PASSWORD__;
    //$this->baseUrl = 'https://dolce-gusto.sahabatnestlerewards.co.id';
    $this->baseUrl = __API_BASE__;
    $this->oauth2_client_id = __SSO_CLIENT_ID__;
    $this->oauth2_secret    = __SSO_SECRET__;
    $this->oauth2_token_url = __SSO_TOKEN_URL__;
    $this->oauth2_auth_url  = __SSO_AUTH_URL__;
    $this->oauth2_user_info = __SSO_USER_INFO__;
    $this->oauth2_redirect  = __SSO_REDIRECT__;
    // $this->authPassword = 'NamaKUj4t!';
    // $this->baseUrl = 'https://stag.dolce-gusto.sahabatnestlerewards.co.id';
  }

  public function index(){

  }

  public function checkSession(){

    if(!isset($_SESSION['userData'])){
        // $_SESSION['guestlogin'] = (object) $getData;
      $query_params = array('response_type' => 'code',
                              'action'        => 'login',
                              'client_id'     => $this->oauth2_client_id,
                              'redirect_uri'  => $this->oauth2_redirect,
                              'scope'         => 'user');

        $login_url = $this->oauth2_auth_url . '?' . http_build_query($query_params);
        $redirectUrl = $login_url;
        // $redirectUrl = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/home';
        header('Location: '.$redirectUrl); exit;
    }
  }

  public function updateprofile(){
    $this->checkSession();
    $userData = $_SESSION['userData'];

    $free_time2 = $_POST['hear'];
    $free_time = implode(',', $free_time2);
    // print_r($_POST['g-recaptcha-response']);
    // $client = \Drupal::service('http_client');
    // $result = $client->post('https://www.google.com/recaptcha/api/siteverify',
    //                           ['form_params' => [
    //                             'secret' => '6Lc5lR4UAAAAAEkvlGnl5jgo5rY6L5NZIZpz3wDu',
    //                             'response' => $_POST['g-recaptcha-response']
    //                           ]
    //                         ]);
    // $output = json_decode($result->getBody());
    // // print_r($output->success);exit;
    // if($output->success){
//print_r($_POST);exit;
    if($_POST['newsletter'] == 1){
      $new_type2 = $_POST['newsletter_type'];
      $new_type = implode(',', $new_type2);
    }else{
      $new_type = "";
    }
      $client = \Drupal::service('http_client');
      $result = $client->post($this->baseUrl.'public/user/update',
                              ['form_params' => [
                                'id' => $userData->id,
                                'title' => $_POST['title'],
                                'firstname' => $_POST['firstname'],
                                'lastname' => $_POST['lastname'],
                                //'email' => $_POST['email'],
                                'phone' => $_POST['phone'],
                                'birthday' => $_POST['birthday'],
                                'occupation' => $_POST['occupation'],
                                'address' => $_POST['adrress'],
                                'province' => $_POST['province'],
                                'city' => $_POST['city'],
                                'district' => $_POST['district'],
                                'newsletter' => $_POST['newsletter'],
                                //'family' => $_POST['status'],
                                'free_time' => $free_time,
                                'newsletter_type' => $new_type,
                                // 'consume_dolcegusto' => $_POST['consume_dolcegusto'],
                                // 'have_machine' => $_POST['have_machine'],
                                // 'register_machine' => $_POST['register_machine'],
                                'postal_code' => $_POST['postal_code'],
                              ],
                              'auth' => [$this->authUsername,$this->authPassword]
                            ]);

      $output = json_decode($result->getBody());
      //print_r($output);exit;
      // $url = Url::fromRoute('custompage.profile');
      $redirect_to = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/profile';

      return new \Drupal\Core\Routing\TrustedRedirectResponse($redirect_to);
      //return new \Symfony\Component\HttpFoundation\RedirectResponse('/guest/profile');
    // }else{
    //   return new \Symfony\Component\HttpFoundation\RedirectResponse('/guest/editprofile');
    // }
  }

  public function updatepoint(){
    $this->checkSession();
    $userData = $_SESSION['userData'];

    $validators = array();
    $source = $_FILES["receipt"]["tmp_name"];

    $check = getimagesize($source);

    $target_dir = drupal_realpath() . "/dumy/";
    $target_file = $target_dir . basename($_FILES["receipt"]["name"]);
    $getFile = '';
    if($check !== false) {
         $file = move_uploaded_file($source, $target_file);
          if($file){
            $getFile = fopen($target_file, 'r');
          }else{
            $getFile = '';
          }
    } else {
        $getFile = '';
    }
    
    // echo '<pre>';
    // print_r(array(['multipart' => [
    //                 [
    //                   'name' => 'image',
    //                   'contents' => $getFile,
    //                 ],
    //                 [
    //                   'name' => 'user_id',
    //                   'contents' => $userData->id,
    //                 ],
    //                 [
    //                   'name' => 'promo_code',
    //                   'contents' => $_POST['promo_code'],
    //                 ],
    //                 [
    //                   'name' => 'place_purchase',
    //                   'contents' => $_POST['place_name'],
    //                 ],
    //                 [
    //                   'name' => 'store',
    //                   'contents' => $_POST['store_name'],
    //                 ]
    //               ]]));
    // echo '</pre>';
    // exit;


    $client = \Drupal::service('http_client');
    $result = $client->request('POST', $this->baseUrl.'public/receipt/upload', ['multipart' => [
                    [
                      'name' => 'image',
                      'contents' => $getFile,
                    ],
                    [
                      'name' => 'user_id',
                      'contents' => (string) $userData->id,
                    ],
                    [
                      'name' => 'promo_code',
                      'contents' => $this->filterinput($_POST['promo_code']),
                    ],
                    [
                      'name' => 'unique_code',
                      'contents' => $this->filterinput(strtoupper($_POST['unique_code'])),
                    ],
                    [
                      'name' => 'place_purchase',
                      'contents' => $this->filterinput($_POST['place_name']),
                    ],
                    [
                      'name' => 'store',
                      'contents' => $this->filterinput($_POST['store_name']),
                    ]
                  ],
                  'auth' => [$this->authUsername,$this->authPassword]
                ]);
    $output = json_decode($result->getBody());
    // print_r($output->message);
    // exit;
    // $url = Url::fromRoute('custompage.profile');

    if($output->status == '0'){
      $urlRedirect = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/submitpoint?message=1000';
    }else if($output->status == '1'){
      $urlRedirect = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/submitpoint?message=1001';
    }else if($output->status == '2'){
      $urlRedirect = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/submitpoint?message=1002';
    }else if($output->status == '3'){
      $urlRedirect = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/submitpoint?message=1003';
    }else if($output->status == '4'){
      $urlRedirect = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/submitpoint?message=1004';
    }else{
      $urlRedirect = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/submitpoint?message=1005';
    }

    //$redirect_to = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/profile';

    return new \Drupal\Core\Routing\TrustedRedirectResponse($urlRedirect);

    //return new \Symfony\Component\HttpFoundation\RedirectResponse($urlRedirect);
  }

  public function addtocart(){

    $userData = $_SESSION['userData'];
    $data_cart = json_decode($_POST['data_cart']);
    $response = 'Something Error Please Try Again';
    $client = \Drupal::service('http_client');
    $result = $client->post($this->baseUrl.'public/redeem/cart',
                            ['form_params' => [
                              'user_id' => $userData->id,
                              'data_cart' => $data_cart,
                            ],
                            'auth' => [$this->authUsername,$this->authPassword]
                          ]);
    $output = json_decode($result->getBody());
    //print_r($output);exit;
    if($output){
      $response = $output->data;
    }
    return new Response(json_encode($output));
    //return new Response($output->data);
  }

  public function removecart(){

    $userData = $_SESSION['userData'];
    $cartID = $_POST['cartID'];
    $response = 'Something Error Please Try Again';
    $client = \Drupal::service('http_client');
    $result = $client->post($this->baseUrl.'public/redeem/delete',
                            ['form_params' => [
                              'id' => $cartID,
                            ],
                            'auth' => [$this->authUsername,$this->authPassword]
                          ]);
    $output = json_decode($result->getBody());
    if($output){
      $response = $output->data;
    }

    return new Response($output->data);
  }

  public function selectdistrict(){

    $userData = $_SESSION['userData'];

    $idcity = $_POST['idcity'];
    $response = 'Something Error Please Try Again';
    $client = \Drupal::service('http_client');
    $result = $client->post($this->baseUrl.'public/list_data/district',
                            ['form_params' => [
                              'city' => $idcity,
                            ],
                            'auth' => [$this->authUsername,$this->authPassword]
                          ]);
    $output = json_decode($result->getBody());
    if($output){
      $response = $output->data;
    }
    return new Response(json_encode($output->data));
  }

  public function selectstore(){
    //echo "a";exit;
    $userData = $_SESSION['userData'];

    $idcity = $_POST['idcity'];
    //echo $idcity;exit;
    $response = 'Something Error Please Try Again';
    $client = \Drupal::service('http_client');
    $result = $client->post($this->baseUrl.'public/data/store',
                            ['form_params' => [
                              'channel' => $idcity,
                            ],
                            'auth' => [$this->authUsername,$this->authPassword]
                          ]);
    $output = json_decode($result->getBody());
    if($output){
      $response = $output->data;
    }
    return new Response(json_encode($output->data));
  }

  public function submitcontactus(){

    $source = $_FILES["attach"]["tmp_name"];
    //print_r($_FILES["attach"]["tmp_name"]);exit;
    $check = getimagesize($source);

    $target_dir = drupal_realpath() . "/dumy/";
    $target_file = $target_dir . basename($_FILES["attach"]["name"]);
    $getFile = '';
    if($check !== false) {
         $file = move_uploaded_file($source, $target_file);
          if($file){
            $getFile = fopen($target_file, 'r');
          }else{
            $getFile = '';
          }
    } else {
        $getFile = '';
    }

    $client = \Drupal::service('http_client');
    $result = $client->request('POST', $this->baseUrl.'public/user/send_email', ['multipart' => [
                    [
                      'name' => 'image',
                      'contents' => $getFile,
                    ],
                    [
                      'name' => 'name',
                      'contents' => $this->filterinput($_POST['name']),
                    ],
                    [
                      'name' => 'email',
                      'contents' => $this->filterinput($_POST['email']),
                    ],
                    [
                      'name' => 'subject',
                      'contents' => $this->filterinput($_POST['subject']),
                    ],
                    [
                      'name' => 'message',
                      'contents' => $this->filterinput($_POST['message']),
                    ],
                    [
                      'name' => 'telp',
                      'contents' => $this->filterinput($_POST['telp']),
                    ]
                  ],
                  'auth' => [$this->authUsername,$this->authPassword]
                ]);
    $output = json_decode($result->getBody());


    // $name = $_POST['name'];
    // $email = $_POST['email'];
    // $subject = $_POST['subject'];
    // $message = $_POST['message'];
    // $telp = $_POST['telp'];
    // $client = \Drupal::service('http_client');
    // $result = $client->post($this->baseUrl.'public/user/send_email',
    //                         ['form_params' => [
    //                           'name' => $name,
    //                           'email' => $email,
    //                           'subject' => $subject,
    //                           'message' => $message,
				// 'telp' => $telp,
    //                         ],
    //                         'auth' => [$this->authUsername,$this->authPassword]
    //                       ]);
    // $output = json_decode($result->getBody());
    $urlRedirect = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/contactus?error='.$output->status;

    return new \Drupal\Core\Routing\TrustedRedirectResponse($urlRedirect);

    //return new \Symfony\Component\HttpFoundation\RedirectResponse('/contactus?error='.$output->status);
  }

  public function nextredeem(){
    $this->checkSession();
    $client = \Drupal::service('http_client');
    $userData = $_SESSION['userData'];

    $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
    $resultUser = json_decode($getUserInfo->getBody());

    //check step redeem yang sekarang

    if($resultUser->data->step_redeem == 1){
      //go to halman 2
      $result = $client->post($this->baseUrl.'public/redeem/address',
                              ['form_params' => [
                                'user_id' => $userData->id,
                              ],
                              'auth' => [$this->authUsername, $this->authPassword]
                            ]);
      $output = json_decode($result->getBody());
    }else if($resultUser->data->step_redeem == 2){
      //go to halman 3
      if($_POST['type'] == "addnew"){
        // $_POST['addresstext'];
        // $_POST['patokan'];
        // $_POST['province'];
        // $_POST['district'];
        // $_POST['city'];
        // $_POST['kelurahan'];
        // $_POST['postal_code'];
        // $_POST['phone'];
        // $_POST['saveaddress'];

        $result = $client->post($this->baseUrl.'public/redeem/addAddress',
                                ['form_params' => [
                                  'user_id' => $userData->id,
                                  'address_id' => 0,
                                  'name' => $_POST['name'],
                                  'address' => $_POST['addresstext'],
                                  'patokan' => "",
                                  'province' => $_POST['province'],
                                  'district' => $_POST['district'],
                                  'city' => $_POST['city'],
                                  // 'kelurahan' => $_POST['kelurahan'],
                                  'postal_code' => $_POST['postal_code'],
                                  'phone' => $_POST['phone'],
                                  'temporary' => 1,
                                ],
                                'auth' => [$this->authUsername, $this->authPassword]
                              ]);
        $output = json_decode($result->getBody());
      }else{
        if($_POST['address']){
        $result = $client->post($this->baseUrl.'public/redeem/addAddress',
                                ['form_params' => [
                                  'user_id' => $userData->id,
                                  'address_id' => $_POST['address'],
                                  'address' => '',
                                  'patokan' => '',
                                  'province' => '',
                                  'district' => '',
                                  'city' => '',
                                  // 'kelurahan' => '',
                                  'postal_code' => '',
                                  'phone' => '',
                                  'temporary' => '',
                                ],
                                'auth' => [$this->authUsername, $this->authPassword]
                              ]);
        $output = json_decode($result->getBody());
        }else{
          $redirect_to = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/redeem';
          return new \Drupal\Core\Routing\TrustedRedirectResponse($redirect_to);
        }
      }

    }else if($resultUser->data->step_redeem == 3){
      //go to finish
      $result = $client->post($this->baseUrl.'public/redeem/order',
                              ['form_params' => [
                                'user_id' => $userData->id,
                              ],
                              'auth' => [$this->authUsername, $this->authPassword]
                            ]);
      $output = json_decode($result->getBody());
    }

    $redirect_to = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/redeem';

    return new \Drupal\Core\Routing\TrustedRedirectResponse($redirect_to);

    //return new \Symfony\Component\HttpFoundation\RedirectResponse('/guest/redeem');
  }

  public function prevredeem(){
    $this->checkSession();
    $client = \Drupal::service('http_client');
    $userData = $_SESSION['userData'];

    $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
    $resultUser = json_decode($getUserInfo->getBody());

    //check step redeem yang sekarang

    if($resultUser->data->step_redeem == 2){
      //go to halman 1
      $result = $client->post($this->baseUrl.'public/redeem/back',
                              ['form_params' => [
                                'user_id' => $userData->id,
                              ],
                              'auth' => [$this->authUsername, $this->authPassword]
                            ]);
      $output = json_decode($result->getBody());
    }else if($resultUser->data->step_redeem == 3){
      //go to halman 2
      $result = $client->post($this->baseUrl.'public/redeem/back',
                              ['form_params' => [
                                'user_id' => $userData->id,
                              ],
                              'auth' => [$this->authUsername, $this->authPassword]
                            ]);
      $output = json_decode($result->getBody());
    }
    $redirect_to = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/guest/redeem';

    return new \Drupal\Core\Routing\TrustedRedirectResponse($redirect_to);
  }

  public function upload_profile_photo(){
       $this->checkSession();
        $userData = $_SESSION['userData'];
        // $getFilePath = $this->fileUpload($_FILES["imgupload"]["name"], $_FILES["imgupload"]["tmp_name"], $_FILES["imgupload"]["size"]);
       $validators = array();
       $source = $_FILES["imgupload"]["tmp_name"];

        // $getName = explode('/', $getFilePath);
        // $getName = end($getName);
      
        $check = getimagesize($source);

        $target_dir = drupal_realpath() . "/temp/";
        $target_file = $target_dir . basename($_FILES["imgupload"]["name"]);
        $getFile = '';
        if($check !== false) {
             $file = move_uploaded_file($source, $target_file);
              if($file){
                $getFile = fopen($target_file, 'r');
              }else{
                $getFile = '';
              }
        } else {
            $getFile = '';
       }

    $client = \Drupal::service('http_client');
    $result = $client->request('POST', $this->baseUrl.'public/user/upload', ['multipart' => [
                    [
                      'name' => 'profilepic',
                      'contents' => $getFile
                    ],[
                      'name' => 'user_id',
                      'contents' => (string) $userData->id,
                    ]
                  ],
                  'auth' => [$this->authUsername,$this->authPassword]
                ]);
    $output = json_decode($result->getBody());
    return new \Symfony\Component\HttpFoundation\RedirectResponse('/guest/profile');
  }
        // $query = \Drupal::database()->update('users_tb');
        // $query->fields([
        //         'profilepic' => $getName
        //     ]);
        // $query->condition('id', $userData->id);
        // $query->execute();

        // // db_query("UPDATE users_tb SET profilepic = '$getName' WHERE id = $userData->id");

        // if(!$query->execute()){
        //     $query = \Drupal::database()->insert('users_tb');
        //     $query->fields(['member_id', 'profilepic']);
        //     $query->values([
        //         $this->getSession()->id,
        //         $getName
        //     ]);
        //     $query->execute();
        // }
        // //update session
        // $_SESSION['userData']->profilepic = $getFilePath;

        // return new Response($getFilePath);
    // }

  function fileUpload($filename, $tmpfilename, $filesize){

        // Check if image file is a actual image or fake image
        // if(isset($_POST["submit"])) {

        $response = '';
        $check = getimagesize($tmpfilename);
        if($check !== false) {
            $response .= "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            $response .= "File is not an image.";
            $uploadOk = 0;
        }
        // }


        $imageFileType = pathinfo($filename, PATHINFO_EXTENSION);

        $target_dir = "./temp/";
        $target_dir2 = "/temp/";
        $renameFile = uniqid() . '.' . $imageFileType;
        $target_file = $target_dir . $renameFile;
        $uploadOk = 1;
        // $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        // Check if file already exists
        if (file_exists($target_file)) {
            $response .= "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        // if ($filesize > 500000) {
        //     $response .= "Sorry, your file is too large.";
        //     $uploadOk = 0;
        // }
        // Allow certain file formats
        $allowFile = array('jpg', 'jpeg', 'png');

        if(!in_array($imageFileType, $allowFile)) {
            $response .= "Sorry, only image JPG or PNG files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $response .= "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($tmpfilename, $target_file)) {
                $response = $target_file; //"The file ". basename($filename). " has been uploaded.";
            } else {
                $response .= "Sorry, there was an error uploading your file.";
            }
        }

        if($uploadOk != 0){
            $response = \Drupal::request()->getSchemeAndHttpHost() . __SUBDIR__ . $target_dir2 . $renameFile;
        }else{
            $response = $response;
        }


        return $response;
  }

  public function submitmachine(){

    $userData = $_SESSION['userData'];

    $validators = array();
    $source = $_FILES["image"]["tmp_name"];

    $check = getimagesize($source);

    $target_dir = drupal_realpath() . "/dumy/";
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    $getFile = '';
    if($check !== false) {
         $file = move_uploaded_file($source, $target_file);
          if($file){
            $getFile = fopen($target_file, 'r');
          }else{
            $getFile = '';
          }
    } else {
        $getFile = '';
    }

    $client = \Drupal::service('http_client');
    $result = $client->request('POST', $this->baseUrl.'public/register_machine/upload', ['multipart' => [
                    [
                      'name' => 'image',
                      'contents' => $getFile,
                    ],[
                      'name' => 'user_id',
                      'contents' => (string) $userData->id,
                    ],
                    [
                      'name' => 'serial_number',
                      'contents' => $this->filterinput($_POST['serialnumber']),
                    ],
                    [
                      'name' => 'unique_code',
                      'contents' => $this->filterinput($_POST['uniquecode']),
                    ],
                    [
                      'name' => 'product_id',
                      'contents' => $this->filterinput($_POST['product_id']),
                    ],
                    [
                      'name' => 'get_machine',
                      'contents' => $this->filterinput($_POST['getmachine']),
                    ],
                    [
                      'name' => 'month',
                      'contents' => $this->filterinput($_POST['month']),
                    ],
                    [
                      'name' => 'year',
                      'contents' => $this->filterinput($_POST['year']),
                    ],
                    [
                      'name' => 'place',
                      'contents' => $this->filterinput($_POST['place']),
                    ],
                    [
                      'name' => 'store',
                      'contents' => $this->filterinput($_POST['store']),
                    ]
                  ],
                  'auth' => [$this->authUsername,$this->authPassword]
                ]);
    $output = json_decode($result->getBody());

    // $url = Url::fromRoute('custompage.profile');

    return new \Symfony\Component\HttpFoundation\RedirectResponse('/guest/registermachine');
  }

  private function filterinput($string, $url=false){
		if($string == '' || !$string){
			return null;
		}
		
		if($url){
			$string = filter_var($string, FILTER_VALIDATE_URL);
		}else{
			$string = filter_var($string, FILTER_SANITIZE_MAGIC_QUOTES);
			// only support for name/username
			$string = preg_replace("/[^a-z 0-9-_'.]+/i", "", $string);
		}
		
		// for sanitize (">) ('>)
		if($string == 34 || $string == 39){
			return null;	
		}
		
		return htmlspecialchars(stripslashes($string));
	}
}
