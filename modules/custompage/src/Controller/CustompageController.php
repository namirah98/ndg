<?php
/**
 * @file
 * Contains \Drupal\custompage\CustompageController.
 */

namespace Drupal\custompage\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Url;
use \Drupal\custompage\Controller\HttpClient;

/**
 * Provides route responses for the Example module.
 */
class CustompageController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  private $authUsername;
  private $authPassword;
  private $baseUrl;
  private $oauth2_client_id;
  private $oauth2_secret;
  private $oauth2_token_url;
  private $oauth2_auth_url;
  private $oauth2_user_info;
  private $oauth2_redirect;

  public function __construct(){
    $this->authUsername = __AUTH_USERNAME__;
    // $this->authPassword = 'u:g?LW1K4j';
    $this->baseUrl = __API_BASE__;
    $this->authPassword = __AUTH_PASSWORD__;
    //$this->baseUrl = 'https://stag.dolce-gusto.sahabatnestlerewards.co.id';

    $this->oauth2_client_id = __SSO_CLIENT_ID__;
    $this->oauth2_secret    = __SSO_SECRET__;
    $this->oauth2_token_url = __SSO_TOKEN_URL__;
    $this->oauth2_auth_url  = __SSO_AUTH_URL__;
    $this->oauth2_user_info = __SSO_USER_INFO__;
    $this->oauth2_redirect  = __SSO_REDIRECT__;
    $base_url = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__;


    $this->query_params = array('response_type' => 'code',
                          'action'        => 'login',
                          'client_id'     => $this->oauth2_client_id,
                          'redirect_uri'  => $this->oauth2_redirect,
                          'scope'         => 'user');

    $this->login_url = $this->oauth2_auth_url . '?' . http_build_query($this->query_params);

    $this->query_params = array('response_type' => 'code',
                          'action'        => 'register',
                          'client_id'     => $this->oauth2_client_id,
                          'redirect_uri'  => $this->oauth2_redirect,
                          'scope'         => 'user');

    $this->register_url = $this->oauth2_auth_url . '?' . http_build_query($this->query_params);

    $this->base_url = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__;

    header("X-XSS-Protection: 1; mode=block");
    header("X-Frame-Options: DENY");
    header("X-Content-Type-Options: nosniff");
  }
  public function index(){

  }

  public function checkSession(){

    if(!isset($_SESSION['userData'])){
        // $_SESSION['guestlogin'] = (object) $getData;
      $query_params = array('response_type' => 'code',
                              'action'        => 'login',
                              'client_id'     => $this->oauth2_client_id,
                              'redirect_uri'  => $this->oauth2_redirect,
                              'scope'         => 'user');

        $login_url = $this->oauth2_auth_url . '?' . http_build_query($query_params);
        $redirectUrl = $login_url;
        // $redirectUrl = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__.'/home';
        header('Location: '.$redirectUrl); exit;
    }
  }


  public function editprofile(){
    $this->checkSession();
    // $session = new \Symfony\Component\HttpFoundation\Session\Session();
    // $userData = $session->get('userData');
    $userData = $_SESSION['userData'];
    //print_r($userData);exit;
    // global $base_url;
    $client = \Drupal::service('http_client');
    //get update user info
    $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
    $resultUser = json_decode($getUserInfo->getBody());

    
    $city = $client->get($this->baseUrl.'public/list_data/city', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
    $resultcity = json_decode($city->getBody());

    $occupation = $client->get($this->baseUrl.'public/list_data/occupation', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
    $resultoccupation = json_decode($occupation->getBody());

    $district = $client->post($this->baseUrl.'public/list_data/district',
                            ['form_params' => [
                              'city' => $resultUser->data->city,
                            ],
                            'auth' => [$this->authUsername,$this->authPassword]]);
    $resultdistrict = json_decode($district->getBody());

    $province = $client->get($this->baseUrl.'public/list_data/province', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
    $resultprovince = json_decode($province->getBody());

    $url = Url::fromRoute('custompage.updateprofile');

    if($resultUser->data->hear){
       $resultUser->data->hear = explode(',', $resultUser->data->hear);
    }
    if($resultUser->data->newsletter_type){
       $resultUser->data->newsletter_type = explode(',', $resultUser->data->newsletter_type);
    }
    if($resultUser->data->address){
       $resultUser->data->address = strip_tags($resultUser->data->address);
    }

    if(!file_exists(drupal_realpath() . "/apidolcegusto/uploads/" . $resultUser->data->profilepic)){
        $resultUser->data->profilepic = '';
      }
    //print_r($resultUser->data);exit;
    $element = array(
      '#theme' => 'editprofile',
      '#userdata' => $resultUser->data,
      '#city' => $resultcity->data,
      '#province' => $resultprovince->data,
      '#district' => $resultdistrict->data,
      '#occupation' => $resultoccupation->data,
      '#actionurl' => $url,
      '#base_url' => $this->base_url,
      '#form' => \Drupal::formBuilder()->getForm('Drupal\custompage\Form\EditprofileForm'),
    );
    return $element;
  }

  public function joinclub(){
    $this->checkSession();
    // $session = new \Symfony\Component\HttpFoundation\Session\Session();
    // $userData = $session->get('userData');
    $userData = $_SESSION['userData'];

    $client = \Drupal::service('http_client');
    //get update user info
    $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
    $resultUser = json_decode($getUserInfo->getBody());

    $city = $client->get($this->baseUrl.'public/list_data/city', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
    $resultcity = json_decode($city->getBody());

    $occupation = $client->get($this->baseUrl.'public/list_data/occupation', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
    $resultoccupation = json_decode($occupation->getBody());

    $district = $client->post($this->baseUrl.'public/list_data/district',
                            ['form_params' => [
                              'city' => $resultUser->data->city,
                            ],
                            'auth' => [$this->authUsername,$this->authPassword]]);
    $resultdistrict = json_decode($district->getBody());

    $province = $client->get($this->baseUrl.'public/list_data/province', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
    $resultprovince = json_decode($province->getBody());

    $url = Url::fromRoute('custompage.updateprofile');

    $element = array(
      //'#theme' => 'joinclub',
      '#theme' => 'editprofile',
      '#userdata' => $resultUser->data,
      '#city' => $resultcity->data,
      '#province' => $resultprovince->data,
      '#district' => $resultdistrict->data,
      '#occupation' => $resultoccupation->data,
      '#actionurl' => $url,
      '#base_url' => $this->base_url,
      '#form' => \Drupal::formBuilder()->getForm('Drupal\custompage\Form\EditprofileForm'),
    );
    return $element;
  }

  public function profile() {
    $this->checkSession();

    // $session = new \Symfony\Component\HttpFoundation\Session\Session();
    // $userData = $session->get('userData');
    $userData = $_SESSION['userData'];
    //     echo "<pre>";
    // print_r($userData);
    // echo "</pre>";exit; 
    // print_r($userData);
    // exit;
    // $base_url = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__;

    if($userData){
      $client = \Drupal::httpClient();
      //get update user info
      $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $resultUser = json_decode($getUserInfo->getBody());

      
      $element = array(
        '#theme' => 'profile',
        '#userdata' => $resultUser->data,
        '#base_url' => $this->base_url
      );
      return $element;
    }else{
      return new \Drupal\Core\Routing\TrustedRedirectResponse($this->login_url);
    }
  }

  public function pointbalance() {
    $this->checkSession();
    // $session = new \Symfony\Component\HttpFoundation\Session\Session();
    // $userData = $session->get('userData');
    $userData = $_SESSION['userData'];
    // print_r($userData);
    // exit;

    if($userData){

      if(isset($_POST['start_date']) && isset($_POST['end_date'])){
        $start_date = date('Y-m-d',strtotime($_POST['start_date']));
        $end_date = date('Y-m-d',strtotime($_POST['end_date']));
        // print_r(array(
        //                           'user_id' => $userData->id,
        //                           'start_date' => $start_date,
        //                           'end_date' => $end_date
        //                         )); exit;
        $client = \Drupal::service('http_client');
        $result = $client->post($this->baseUrl.'public/user_point/list_history',
                                ['form_params' => [
                                  'user_id' => $userData->id,
                                  'start_date' => $start_date,
                                  'end_date' => $end_date
                                ],
                                'auth' => [$this->authUsername,$this->authPassword]
                              ]);
        $resultPoint = json_decode($result->getBody());

        $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $resultUser = json_decode($getUserInfo->getBody());

        if(!file_exists(drupal_realpath() . "/apidolcegusto/uploads/" . $resultUser->data->profilepic)){
        $resultUser->data->profilepic = '';
        }
        $element = array(
          '#theme' => 'pointbalance',
          '#userdata' => $resultUser->data,
          '#listdata' => $resultPoint->data,
          '#base_url' => $this->base_url
        );
      }else{
        $client = \Drupal::httpClient();
        $getPoint = $client->get($this->baseUrl.'public/user_point/list/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $resultPoint = json_decode($getPoint->getBody());

        $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $resultUser = json_decode($getUserInfo->getBody());

        if(!file_exists(drupal_realpath() . "/apidolcegusto/uploads/" . $resultUser->data->profilepic)){
        $resultUser->data->profilepic = '';
        }

        $element = array(
          '#theme' => 'pointbalance',
          '#userdata' => $resultUser->data,
          '#listdata' => $resultPoint->data,
          '#base_url' => $this->base_url
        );
      }
      return $element;
    }else{
      return new \Drupal\Core\Routing\TrustedRedirectResponse($this->login_url);
    }
  }

  public function getRefferalCode(){
    $_SESSION['referal_code'] = $_GET['code'];
    return new \Drupal\Core\Routing\TrustedRedirectResponse($this->register_url);
  }

  public function referal() {
    // $session = new \Symfony\Component\HttpFoundation\Session\Session();
    // $userData = $session->get('userData');
    // global $base_url;
    $userData = $_SESSION['userData'];
    // print_r($userData);
    // exit;

    if($userData){
      $client = \Drupal::httpClient();
      //get update user info
      $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $resultUser = json_decode($getUserInfo->getBody());

      $element = array(
        '#theme' => 'referal',
        '#userdata' => $resultUser->data,
        '#base_url' => $this->base_url,
      );
      return $element;
    }else{
      return new \Drupal\Core\Routing\TrustedRedirectResponse($this->login_url);
    }
  }

  public function registermachine() {
    // $session = new \Symfony\Component\HttpFoundation\Session\Session();
    // $userData = $session->get('userData');
    $userData = $_SESSION['userData'];
    $_SESSION['saveuniquecode'] = isset($_POST['uniquecode']) ? $_POST['uniquecode'] : '';
    // print_r($userData);
    // exit;

    if($userData){
      $getuniquecode = $_SESSION['saveuniquecode'];
      $client = \Drupal::service('http_client');
      //get update user info
      $getMachine = $client->get($this->baseUrl.'public/product/machineList', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $resultMachine = json_decode($getMachine->getBody());

      $url = Url::fromRoute('custompage.submitmachine');

      $element = array(
        '#theme' => 'registermachine',
        '#machine' => $resultMachine->data,
        '#apiurl' => $this->baseUrl,
        '#uniquecode' => $getuniquecode,
        '#actionurl' => $url,
      );
      return $element;
    }else{
      return new \Drupal\Core\Routing\TrustedRedirectResponse($this->login_url);
    }
  }

  public function submitpoint(){
    $this->checkSession();
    // $session = new \Symfony\Component\HttpFoundation\Session\Session();
    // $userData = $session->get('userData');
    $userData = $_SESSION['userData'];

    if($userData){
      $client = \Drupal::service('http_client');
      //get update user info
      $getUserInfo = $client->get($this->baseUrl.'public/user_point/list_receipt/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $resultUser = json_decode($getUserInfo->getBody());

      $getUserInfo1 = $client->get($this->baseUrl.'public/data/channel', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $resultUser1 = json_decode($getUserInfo1->getBody());

      $getUserInfo2 = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $resultUser2 = json_decode($getUserInfo2->getBody());
      //print_r($resultUser1);
      $getUserInfo3 = $client->get($this->baseUrl.'public/data/lokasi', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $resultUser3 = json_decode($getUserInfo3->getBody());

      $url = Url::fromRoute('custompage.updatepoint');

      if(!file_exists(drupal_realpath() . "/apidolcegusto/uploads/" . $resultUser2->data->profilepic)){
        $resultUser2->data->profilepic = '';
      }

      $element = array(
        '#theme' => 'submitpoint',
        '#userdata' => $resultUser2->data,
        '#lokasi' => $resultUser3->data,
        '#actionurl' => $url,
        '#summary' => $resultUser->data,
        '#api_url' => $this->baseUrl,
        '#base_url' => $this->base_url,
        '#channel' => $resultUser1
      );
      return $element;
    }else{
      return new \Drupal\Core\Routing\TrustedRedirectResponse($this->login_url);
    }
  }

  public function redeem(){
    $this->checkSession();
    // $session = new \Symfony\Component\HttpFoundation\Session\Session();
    // $userData = $session->get('userData');
    $userData = $_SESSION['userData'];
    // print_r($userData);
    // exit;

    if($userData){
      $client = \Drupal::httpClient();
      //get update user info
      $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $resultUser = json_decode($getUserInfo->getBody());

      // $getRedeem = $client->post($this->baseUrl.'public/redeem/list/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      // $resulRedeem = json_decode($getRedeem->getBody());

      $getRedeem = $client->post($this->baseUrl.'public/redeem/list',
                            ['form_params' => [
                              'user_id' => $userData->id,
                            ],
                            'auth' => [$this->authUsername,$this->authPassword]
                          ]);
      $resulRedeem = json_decode($getRedeem->getBody());

      $getAddress = $client->post($this->baseUrl.'public/redeem/addressList',
                            ['form_params' => [
                              'user_id' => $userData->id,
                            ],
                            'auth' => [$this->authUsername,$this->authPassword]
                          ]);
      $resultAddress = json_decode($getAddress->getBody());
      // echo "<pre>";
      // print_r($resultAddress);
      // echo "<pre>";exit;
      $dataConfirmation = $client->post($this->baseUrl.'public/redeem/confirmation',
                            ['form_params' => [
                              'user_id' => $userData->id,
                            ],
                            'auth' => [$this->authUsername,$this->authPassword]
                          ]);
      $resultConfirmation = json_decode($dataConfirmation->getBody());

        $city = $client->get($this->baseUrl.'public/list_data/city', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $resultcity = json_decode($city->getBody());

        $district = $client->post($this->baseUrl.'public/list_data/district',
                                ['form_params' => [
                                  'city' => $resultUser->data->city,
                                ],
                                'auth' => [$this->authUsername,$this->authPassword]]);
        $resultdistrict = json_decode($district->getBody());

        $province = $client->get($this->baseUrl.'public/list_data/province', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $resultprovince = json_decode($province->getBody());

      $element = array();

      if(!file_exists(drupal_realpath() . "/apidolcegusto/uploads/" . $resultUser->data->profilepic)){
        $resultUser->data->profilepic = '';
      }

      if($resultUser->data->step_redeem == 1){
        $element = array(
          '#theme' => 'redeem_point_1',
          '#cart' => $resulRedeem->data,
          '#point' => $resultUser->data,
          '#base_url' => $this->base_url,
        );

      }else if($resultUser->data->step_redeem == 2){
        $element = array(
          '#theme' => 'redeem_point_2',
          '#address' => $resultAddress->data,
          '#point' => $resultUser->data,
          '#city' => $resultcity->data,
          '#province' => $resultprovince->data,
          '#district' => $resultdistrict->data,
          '#cart' => $resulRedeem->data,
          '#base_url' => $this->base_url,
        );

      }else if($resultUser->data->step_redeem == 3){
        // echo "<pre>";
        // print_r($resultConfirmation);
        // echo "</pre>";exit;
        $element = array(
          '#point' => $resultUser->data,
          '#theme' => 'redeem_point_3',
          '#success' => $resultConfirmation->data,
          '#cart' => $resulRedeem->data,
          '#base_url' => $this->base_url,
        );

      }else if($resultUser->data->step_redeem == 4){
        $lastStep = $client->post($this->baseUrl.'public/redeem/thankyou',
                              ['form_params' => [
                                'user_id' => $userData->id,
                              ],
                              'auth' => [$this->authUsername,$this->authPassword]
                            ]);
        $resultLastStep = json_decode($lastStep->getBody());

        $element = array(
          '#theme' => 'redeem_finish',
          '#point' => $resultUser->data,
          '#base_url' => $this->base_url,
        );

      }

      return $element;
    }else{
      return new \Drupal\Core\Routing\TrustedRedirectResponse($this->login_url);
    }
  }

  public function dashboard() {

    // $tempstore = \Drupal::service('user.private_tempstore')->get('customepage');
    // $userData = $tempstore->get('userData');
    // $session = new \Symfony\Component\HttpFoundation\Session\Session();
    // $userData = $session->get('userData');
    $userData = $_SESSION['userData'];
    // print_r($userData);
    // exit;
    if($userData){
      $client = \Drupal::service('http_client');
      $result = $client->get($this->baseUrl.'public/user_point/list/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $output = json_decode($result->getBody());
      $text = '';
      $user_info = '';
      foreach ($output->data as $data) {
        $text .= '<tr>';
        $text .= '<td>'.$data->created_at.'</td>';
        $text .= '<td>'.($data->type==1 ? '+' : '-').'</td>';
        $text .= '<td>'.$data->description.'</td>';
        $text .= '<td>'.$data->point.'</td>';
        $text .= '</tr>';
      }

      //get update user info
      $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $resultUser = json_decode($getUserInfo->getBody());

      // $tempstore = \Drupal::service('user.private_tempstore')->get('customepage');
      // $tempstore->set('userData', $resultUser->data);

      $tempstore = new \Symfony\Component\HttpFoundation\Session\Session();
      $tempstore = $session->set('userData', $resultUser->data);
      $newUserData = $tempstore->get('userData');

      $user_info .= 'User ID : '.$newUserData->id.'<br>';
      $user_info .= 'Username : '.$newUserData->username.'<br>';
      $user_info .= 'Email : '.$newUserData->email.'<br>';
      $user_info .= 'Point : '.$newUserData->point.'<br>';
      // $serializer = \Drupal::service('serialize');
      // $entity = $serializer->deserialize($output, 'Drupal\node\Entity\Node', $format);
      $getLink = \Drupal::request()->getSchemeAndHttpHost().'/guest/logout';//Url::fromRoute('custompage.logout');
      // print_r($getLink);exit;
      $logout_url = '<a href="'.$getLink.'" class="button js-form-submit form-submit">Logout</a>';
      $element = array(
        // '#markup' => '<pre>'.print_r($text).'</pre>',
        '#theme' => 'dashboard',
        '#point_history' => $this->t($text),
        '#form' => \Drupal::formBuilder()->getForm('Drupal\custompage\Form\DashboardForm'),
        '#user_info' => $this->t($user_info),
        '#logout_url' => $this->t($logout_url),
      );

      return $element;
    }else{
      return new \Drupal\Core\Routing\TrustedRedirectResponse($this->login_url);
    }
  }

  public function rewards() {
      $userData = $_SESSION['userData'];

      $client = \Drupal::service('http_client');
      $result = $client->get($this->baseUrl.'public/product_rewards_category/list_active', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $output = json_decode($result->getBody());

      if($userData){
        $point = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $getPoint = json_decode($point->getBody());

        $getUserData = $getPoint->data;
      }else{
        $getUserData = '';
      }

      // $base_url = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__;
      $class = array('blue','green','pink','brown');
      $element = array(
        // '#markup' => '<pre>'.print_r($text).'</pre>',
        '#theme' => 'rewards',
        '#product_rewards' => $output->data,
        '#class' => $class,
        '#base_url' => $this->base_url,
        '#point' => $getUserData,
        '#apiurl' => $this->baseUrl,
      );

      // <a href="'.$base_url.'/rewards/'.$data->alias.'">
      //                 <img width="200" src="http://dolce-gusto.iris-development.infouploads/'.$data->image.'">
      //                 <div><strong>'.$data->title.'</strong></div>

      return $element;
  }

  function product_rewards($alias){
    $userData = $_SESSION['userData'];

      $client = \Drupal::service('http_client');
      $result = $client->get($this->baseUrl.'public/product_rewards/product/'.$alias, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $output = json_decode($result->getBody());
      // echo '<pre>';
      // print_r($output->data->detail_category);
      // echo '</pre>';
      // exit;
      if($userData){
        $point = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $getPoint = json_decode($point->getBody());

        $getUserData = $getPoint->data;
      }else{
        $getUserData = '';
      }

      if($output->data->detail_category){
        if($output->data->detail_category->category == 0){
        //   // echo '<pre>';
        //   // print_r($output);
        //   // echo '</pre>'; exit;
          // $base_url = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__;

        //   $text = '<img width="200" src="http://dolce-gusto.iris-development.infouploads/'.$output->data->detail_category->image.'">
        //                   <div><strong>'.$output->data->detail_category->title.'</strong></div>';

        //   $text .= '<table>
        //             <thead>
        //               <tr>
        //                 <td>Rewards</td>
        //                 <td>Kode</td>
        //                 <td>Poin</td>
        //                 <td>Jumlah</td>
        //               </tr>
        //             </thead>';
        //   foreach ($output->data->product_list as $data) {
        //     $option = '';
        //     for($i=1;$i<=$data->quantity;$i++){
        //       $option .= '<option value='.$i.'">'.$i.'</option>';
        //     }
        //     $text .= '<tr>
        //                 <td>'.$data->name.'</td>
        //                 <td>'.$data->code.'</td>
        //                 <td>'.$data->default_point.'</td>
        //                 <td>
        //                   <select name="qty">
        //                     '.$option.'
        //                   </select>
        //                 </td>
        //               </tr>';
        //   }
        //   $text .= '</table>';
          //echo "a";exit;
          foreach ($output->data->product_list as $data) {
            $getImage = unserialize($data->media);

            foreach ($getImage as $getImg) {
              if($getImg->type == 1){
                $getImg = $getImg->data; break;
              }
            }

            $data->media = $getImg;
          }
          // echo '<pre>';
          // print_r($output->data);
          // echo '</pre>';
          // exit;

          $element = array(
            // '#markup' => '<pre>'.print_r($text).'</pre>',
            //'#theme' => 'product_rewards',
            '#theme' => 'rewards_listmany',
            '#product_rewards' => $output->data->product_list,
            '#product_data' => $output->data->detail_category,
            '#point' => $getUserData,
            '#base_url' => $this->base_url,
            '#apiurl' => $this->baseUrl,
          );

          return $element;
        }else{
          //echo "a";exit;
          // echo '<pre>';
          // print_r($output->data);
          // echo '</pre>';
          // exit;

          $element = array(
              // '#markup' => '<pre>'.print_r($text).'</pre>',
              '#theme' => 'product_rewardsmany',
              //'#theme' => 'rewards_list',
              '#product_data' => $output->data,
              '#point' => $getUserData,
              '#base_url' => $this->base_url,
              '#apiurl' => $this->baseUrl,
            );
            return $element;
          //throw new NotFoundHttpException();
        }
      }else{
        $result = $client->get($this->baseUrl.'public/product_rewards/product_detail/'.$alias, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $output = json_decode($result->getBody());
        $getImage = unserialize($output->data->media);
        $output->data->media = $getImage;
        // echo '<pre>';
        // print_r($output);
        // echo '</pre>';
        // exit;

        $element = array(
            // '#markup' => '<pre>'.print_r($text).'</pre>',
            '#theme' => 'product_rewards',
            //'#theme' => 'rewards_list',
            '#product_data' => $output->data,
            '#point' => $getUserData,
            '#base_url' => $this->base_url,
            '#apiurl' => $this->baseUrl,
            '#category' => $output->category,
          );
          return $element;
      }
  }

  function contactus(){
    $userData = $_SESSION['userData'];
    $error = $_GET['error'];

    if(isset($error)){
      if($error == 1){
        $errortext = "Your inquiries have been submitted. We will get back to you shortly.";
      }
      else{
        $errortext = "Message not submited";
      }
    }else{
      $errortext = "";
    }
    // print_r($userData);
    // exit;

    if($userData){
      $client = \Drupal::httpClient();
      //get update user info
      $getUserInfo = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $resultUser = json_decode($getUserInfo->getBody());

      $element = array(
        '#theme' => 'contactus',
        '#userdata' => $resultUser->data,
        '#error' => $errortext,
        '#base_url' => $this->base_url,
      );
    }else{
      $element = array(
        '#theme' => 'contactus',
        '#userdata' => array(),
        '#error' => $errortext,
        '#base_url' => $this->base_url,
      );
    }

    return $element;
  }

  public function login(){
    $tempstore = \Drupal::service('user.private_tempstore')->get('customepage');
    $userData = $tempstore->get('userData');
    //print_r($userData);exit;
    if($userData){
      throw new NotFoundHttpException();
    }else{
      $element = array(
        // '#markup' => '<pre>'.print_r($text).'</pre>',
        '#theme' => 'login',
        '#form' => \Drupal::formBuilder()->getForm('Drupal\custompage\Form\LoginForm'),
      );
      return $element;
    }
  }

  // public function logout(){
  //   // $tempstore = \Drupal::service('user.private_tempstore')->get('customepage');
  //   // $userData = $tempstore->get('userData');

  //   $userData = $_SESSION['userData'];

  //   if($userData){
  //     // $tempstore = \Drupal::service('user.private_tempstore')->get('customepage');
  //     // $userData = $tempstore->set('userData', '');
  //     unset($_SESSION['userData']);
  //     return new \Symfony\Component\HttpFoundation\RedirectResponse('/');
  //   }else{
  //     throw new NotFoundHttpException();
  //   }
  //   // return new RedirectResponse('/user/logout');
  // }

  public function redirect_to(){
      $info = array();

      // $url = $this->oauth2_token_url;
      // $request = new HttpClient($url);
      // echo $request->haha();

      if (isset($_GET['code'])){
        // try to get an access token
        $code = $_GET['code'];
        // authorization url
        $url = $this->oauth2_token_url;
        // this will be our POST data to send back to the OAuth server in exchange
        // for an access token
        $params = array(
          "code"          => $code,
          "client_id"     => $this->oauth2_client_id,
          "client_secret" => $this->oauth2_secret,
          "redirect_uri"  => $this->oauth2_redirect,
          "grant_type"    => "authorization_code"
        );

        // build a new HTTP POST request
        $request = new HttpClient($url);
        $request->setPostData($params);
        $request->send();

        // decode the incoming string as JSON
        $responseObj = json_decode($request->getHttpResponse());


        // Tada: we have an access token!. You can now use this token to access the userinfo API endpoint.
        $info['token'] = sprintf("Access Token from Server: %s", $responseObj->access_token);

        // user information API endpoint
        $url = $this->oauth2_user_info;

        $request = new HttpClient($url);
        $request->setHeaderData(array(sprintf("Authorization: Bearer %s", $responseObj->access_token)));
        $request->send();

        // decode the incoming string as JSON
        $responseObj = json_decode($request->getHttpResponse());
        $changeUserData = (array) $responseObj;
        $changeUserData['referal_code'] = $_SESSION['referal_code'];

        if (!property_exists($responseObj,'error')) {
          $client = \Drupal::service('http_client');
          $result = $client->post($this->baseUrl.'public/user/insert',
                                  ['form_params' => $changeUserData,
                                  'auth' => [$this->authUsername,$this->authPassword]
                                ]);

          $output = json_decode($result->getBody());
          if($output->status == 1){
            //status 1 dia blum registrasi
            $userGet = $client->get($this->baseUrl.'public/user/get/'.$output->data, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
            $resultUser = json_decode($userGet->getBody());
            $_SESSION['userData'] = $resultUser->data;
            session_regenerate_id();
            $register = true;
          }else{
            //status 0 dia sudah registrasi
            $userGet = $client->get($this->baseUrl.'public/user/get/'.$output->data, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
            $resultUser = json_decode($userGet->getBody());
            $_SESSION['userData'] = $resultUser->data;
            session_regenerate_id();
            $register = false;
          }
        }else{
          $_SESSION['error'] = $responseObj->error;
        }

      }else{
        if (isset($_SESSION['userData'])) {
          unset($_SESSION['userData']);
        }
      }


      if($register){
        $url = 'http://159.65.14.249:8072/sendinblue/sib/send_welcome_email_ndg';
        $param = array(
          'emailto' => $changeUserData['email']
        );
        $resp = $this->curl($url, $param);
        $redirectto = new \Symfony\Component\HttpFoundation\RedirectResponse('/guest/joinclub');
      }else{
        $redirectto = new \Symfony\Component\HttpFoundation\RedirectResponse('/');
      }
      return $redirectto;
  }

  public function term(){
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', 'club')
          ->propertyOrderBy("created", "DESC")
          ->range(0, 10)
          ->execute();

    foreach($entities['node'] as $obj)
    {
        $node = node_load($obj->nid);
        print(l($node->title, "node/" . $node->nid));
    }
  }

  public function insert(){
    // just testing
    $register = true;

    if($register){
        $redirectto = new \Symfony\Component\HttpFoundation\RedirectResponse('/guest/joinclub');
      }else{
        $redirectto = new \Symfony\Component\HttpFoundation\RedirectResponse('/');
      }
      return $redirectto;
  }

    public function faq() {
      $userData = $_SESSION['userData'];

      $client = \Drupal::service('http_client');
      $result = $client->get($this->baseUrl.'public/data/faq', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $output = json_decode($result->getBody());

      if($userData){
        $point = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $getPoint = json_decode($point->getBody());

        $getUserData = $getPoint->data;
      }else{
        $getUserData = '';
      }
      
      // $base_url = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__;
      $element = array(
        // '#markup' => '<pre>'.print_r($text).'</pre>',
        '#theme' => 'faq',
        '#faq' => $output->data,
        '#base_url' => $this->base_url,
        '#point' => $getUserData,
        '#apiurl' => $this->baseUrl,
      );

      // <a href="'.$base_url.'/rewards/'.$data->alias.'">
      //                 <img width="200" src="http://dolce-gusto.iris-development.infouploads/'.$data->image.'">
      //                 <div><strong>'.$data->title.'</strong></div>

      return $element;
  }

  public function terms() {
      $userData = $_SESSION['userData'];

      $client = \Drupal::service('http_client');
      $result = $client->get($this->baseUrl.'public/data/content/1', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $output = json_decode($result->getBody());

      if($userData){
        $point = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $getPoint = json_decode($point->getBody());

        $getUserData = $getPoint->data;
      }else{
        $getUserData = '';
      }


      // $base_url = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__;
      $element = array(
        // '#markup' => '<pre>'.print_r($text).'</pre>',
        '#theme' => 'terms',
        '#faq' => $output->data,
        '#base_url' => $this->base_url,
        '#point' => $getUserData,
        '#apiurl' => $this->baseUrl,
      );

      // <a href="'.$base_url.'/rewards/'.$data->alias.'">
      //                 <img width="200" src="http://dolce-gusto.iris-development.infouploads/'.$data->image.'">
      //                 <div><strong>'.$data->title.'</strong></div>

      return $element;
  }

    public function benefits() {
      $userData = $_SESSION['userData'];

      $client = \Drupal::service('http_client');
      $result = $client->get($this->baseUrl.'public/data/content/3', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $output = json_decode($result->getBody());

      if($userData){
        $point = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $getPoint = json_decode($point->getBody());

        $getUserData = $getPoint->data;
      }else{
        $getUserData = '';
      }


      // $base_url = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__;
      $element = array(
        // '#markup' => '<pre>'.print_r($text).'</pre>',
        '#theme' => 'benefits',
        '#faq' => $output->data,
        '#base_url' => $this->base_url,
        '#point' => $getUserData,
        '#apiurl' => $this->baseUrl,
      );

      // <a href="'.$base_url.'/rewards/'.$data->alias.'">
      //                 <img width="200" src="http://dolce-gusto.iris-development.infouploads/'.$data->image.'">
      //                 <div><strong>'.$data->title.'</strong></div>

      return $element;
  }

  public function privacy_policy() {
      $userData = $_SESSION['userData'];

      $client = \Drupal::service('http_client');
      $result = $client->get($this->baseUrl.'public/data/content/2', ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
      $output = json_decode($result->getBody());

      if($userData){
        $point = $client->get($this->baseUrl.'public/user/get/'.$userData->id, ['Accept' => 'application/json', 'auth' => [$this->authUsername,$this->authPassword]]);
        $getPoint = json_decode($point->getBody());

        $getUserData = $getPoint->data;
      }else{
        $getUserData = '';
      }

      // $base_url = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__;
      $element = array(
        // '#markup' => '<pre>'.print_r($text).'</pre>',
        '#theme' => 'privacy_policy',
        '#faq' => $output->data,
        '#base_url' => $this->base_url,
        '#point' => $getUserData,
        '#apiurl' => $this->baseUrl,
      );

      // <a href="'.$base_url.'/rewards/'.$data->alias.'">
      //                 <img width="200" src="http://dolce-gusto.iris-development.infouploads/'.$data->image.'">
      //                 <div><strong>'.$data->title.'</strong></div>

      return $element;
  }

  public function send_welcome_email(){
    $url = 'http://159.65.14.249:8072/sendinblue/sib/send_welcome_email_ndg';
    $param = array(
      'emailto' => 'bochanonly@gmail.com'
    );
    $resp = $this->curl($url, $param);
    $element = array(
      '#success' => $resp
    );
    return $element;
  }

  private function curl($url, $fields){
    $curl = curl_init();
    curl_setopt_array($curl, array(
       CURLOPT_RETURNTRANSFER => 1,
       CURLOPT_URL => $url,
       CURLOPT_POST => 1,
       CURLOPT_POSTFIELDS => $fields
       /*CURLOPT_PROXY => '159.65.14.249',
    CURLOPT_PROXYUSERPWD => 'getaway:sungguhkauterlaludsu',
    CURLOPT_PROXYPORT => 3128,*/
    ));
    $resp = curl_exec($curl);
    curl_close($curl);
    return $resp;
  }

  public function logout(){
    $redirectUrl = \Drupal::request()->getSchemeAndHttpHost().__SUBDIR__;
    unset($_SESSION['userData']);
    return new \Drupal\Core\Routing\TrustedRedirectResponse($redirectUrl);
  }

  private function filterinput($string, $url=false){
		if($string == '' || !$string){
			return null;
		}
		
		if($url){
			$string = filter_var($string, FILTER_VALIDATE_URL);
		}else{
			$string = filter_var($string, FILTER_SANITIZE_MAGIC_QUOTES);
			// only support for name/username
			$string = preg_replace("/[^a-z 0-9-_'.]+/i", "", $string);
		}
		
		// for sanitize (">) ('>)
		if($string == 34 || $string == 39){
			return null;	
		}
		
		return htmlspecialchars(stripslashes($string));
	}
	
  public static function validate_input_string($string) {
        if ($string == '' || !$string) {
            return null;
        }
        $val = filter_var($string, FILTER_SANITIZE_MAGIC_QUOTES);
        return htmlspecialchars(stripslashes($val));
    }
	
}
