<?php
/**
 * @file
 * Contains \Drupal\example\Form\ExampleForm.
 */

namespace Drupal\custompage\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
/**
 * Implements an example form.
 */
class SubmitpointForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'submitpoint_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['code'] = array(
      '#type' => 'textfield',
    );
    $form['place_name'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['store_name'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['receipt'] = array(
      '#type' => 'file',
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => array('class' => array('btn','btn-primary')),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $session = new \Symfony\Component\HttpFoundation\Session\Session();
    $userData = $session->get('userData');
    print_r($form_state->getValue('receipt'));
    exit;
    // Store new file.
    $newfile = file_save_upload('receipt', array(
      'file_validate_extensions' => array ('jpg jpeg png gif tiff'),
    ));

    if($newfile) {
      $some_location = '/tmp';
      $des = $some_location . '-' . time() . $newfile->filename; // Define the new location and add the time stamp to file name.

      $result = file_copy($newfile, $des, FILE_EXISTS_REPLACE);
      if ($result) {
        // Succeed.
      }
      else {
        // Fail.
      }
      print_r($newfile);
      exit;

    }

    // $client = \Drupal::service('http_client');
    // $result = $client->post('http://dolce-gusto.iris-development.info/apidolcegusto/public/user/update', 
    //                         ['form_params' => [
    //                           'id' => $userData->id,
    //                           'title' => $form_state->getValue('title'),
    //                           'firstname' => $form_state->getValue('firstname'),
    //                           'lastname' => $form_state->getValue('lastname'),
    //                           'email' => $form_state->getValue('email'),
    //                           'phone' => $form_state->getValue('phone'),
    //                         ]]);
    // $output = json_decode($result->getBody());

    // $coeg = array('id' => $userData->id,
    //               'title' => $form_state->getValue('title'),
    //               'firstname' => $form_state->getValue('firstname'),
    //               'lastname' => $form_state->getValue('lastname'),
    //               'email' => $form_state->getValue('email'),
    //               'phone' => $form_state->getValue('phone'),
    //               'birthday' => $form_state->getValue('year').'-'.$form_state->getValue('month').'-'.$form_state->getValue('day'),
    //               'occupation' => $form_state->getValue('occupation'),
    //               'adrress' => $form_state->getValue('adrress'),
    //               'province' => $form_state->getValue('province'),
    //               'city' => $form_state->getValue('city'),
    //               'district' => $form_state->getValue('district'),
    //               'newsletter' => $form_state->getValue('newslatter'),
    //               'family' => $form_state->getValue('status'),
    //               'free_time' => $free_time,
    //               'consume_dolcegusto' => $form_state->getValue('consume_dolcegusto'),
    //               'have_machine' => $form_state->getValue('have_machine'),
    //               'register_machine' => $form_state->getValue('register_machine')
    //               );
    // echo '<pre>';
    // print_r($coeg);
    // echo '</pre>';
    // exit;
    // echo '<pre>';
    // $time = implode(',',$form_state->getValue('time'));
    // print_r($form_state->getValue('firstname').' | '.$form_state->getValue('lastname').' | '.$form_state->getValue('email').' | '.$form_state->getValue('phone').' | '.$form_state->getValue('day').' | '.$form_state->getValue('month').' | '.$form_state->getValue('year').' | '.$form_state->getValue('occupation').' | '.$form_state->getValue('adrress').' | '.$form_state->getValue('province').' | '.$form_state->getValue('city').' | '.$form_state->getValue('district').' | '.$form_state->getValue('newslatter').' | '.$form_state->getValue('status').' | '.$time.' | '.$form_state->getValue('consume_dolcegusto').' | '.$form_state->getValue('have_machine').' | '.$form_state->getValue('register_machine'));
    // echo '</pre>';
    // exit;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //$form_state->setRedirect('custompage.mypage');
  
    $url = Url::fromRoute('custompage.profile');

    $form_state->setRedirectUrl($url);

    //drupal_set_message($this->t('Your phone number is @number', array('@number' => $form_state->getValue('phone_number'))));
    // echo 'hahaha';
  }

}