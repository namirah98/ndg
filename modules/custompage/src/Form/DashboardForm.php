<?php
/**
 * @file
 * Contains \Drupal\example\Form\ExampleForm.
 */

namespace Drupal\custompage\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
/**
 * Implements an example form.
 */
class DashboardForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dashboard_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['unique_code'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Unique Code'),
      '#attributes' => array('class' => array('form-control')),
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => array('class' => array('btn','btn-primary')),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $tempstore = \Drupal::service('user.private_tempstore')->get('customepage');
    $userData = $tempstore->get('userData');

    $client = \Drupal::service('http_client');
    $result = $client->post('http://dolce-gusto.iris-development.info/apidolcegusto/public/user_point/redeem', 
                            ['form_params' => [
                              'user_id' => $userData->id,
                              'unique_code' => $form_state->getValue('unique_code')
                            ]]);
    $output = json_decode($result->getBody());
    // echo '<pre>';
    // print_r($output);
    // echo '</pre>';
    // exit;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //$form_state->setRedirect('custompage.mypage');
  
    $url = Url::fromRoute('custompage.dashboard');

    $form_state->setRedirectUrl($url);

    //drupal_set_message($this->t('Your phone number is @number', array('@number' => $form_state->getValue('phone_number'))));
    // echo 'hahaha';
  }

}