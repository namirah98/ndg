<?php
/**
 * @file
 * Contains \Drupal\example\Form\ExampleForm.
 */

namespace Drupal\custompage\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
/**
 * Implements an example form.
 */
class LoginForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(){
    $this->authUsername = __AUTH_USERNAME__;
    // $this->authPassword = 'u:g?LW1K4j';
    // $this->baseUrl = 'http://dolce-gusto.iris-development.info';
    $this->authPassword = __AUTH_PASSWORD__;
    $this->baseUrl = __API_BASE__;
  }
  public function getFormId() {
    return 'login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['email'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#attributes' => array('class' => array('form-control')),
    );
    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#attributes' => array('class' => array('form-control')),
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Login'),
      '#attributes' => array('class' => array('btn','btn-primary')),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // $form = array('email' => $form_state->getValue('email'),
    //                 'password' => md5($form_state->getValue('password')),
    //               );

    $client = \Drupal::service('http_client');
    $result = $client->post($this->baseUrl.'public/user/login', 
                            ['form_params' => [
                              'email' => $form_state->getValue('email'),
                              'password' => md5($form_state->getValue('password'))
                            ],
                            'auth' => [$this->authUsername,$this->authPassword]]);
    $output = json_decode($result->getBody());
    //print_r($output);exit;
    if($output->status == 1){
      // $tempstore = \Drupal::service('user.private_tempstore')->get('customepage');
      // $userData = $tempstore->set('userData', $output->data);
      // $session = new \Symfony\Component\HttpFoundation\Session\Session();
      // $session->set('userData', $output->data);
      $_SESSION['userData'] = $output->data;

    }
//     print_r($session->get('userData'));
// exit;
    // if (strlen($form_state->getValue('unique_code')) < 3) {
    //   $form_state->setErrorByName('unique_code', $this->t('not found'));
    // }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //$form_state->setRedirect('custompage.mypage');
    // $unique_code = $form_state->getValue('unique_code');

    $url = Url::fromRoute('custompage.profile');

    $form_state->setRedirectUrl($url);

    //drupal_set_message($this->t('Your phone number is @number', array('@number' => $form_state->getValue('phone_number'))));
    // echo 'hahaha';
  }

}