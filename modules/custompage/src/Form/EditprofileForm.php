<?php
/**
 * @file
 * Contains \Drupal\example\Form\ExampleForm.
 */

namespace Drupal\custompage\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
/**
 * Implements an example form.
 */
class EditprofileForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'profile_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['title'] = array(
      '#type' => 'textfield',
    );
    $form['firstname'] = array(
      '#type' => 'textfield',
    );
    $form['lastname'] = array(
      '#type' => 'textfield',
    );
    $form['email'] = array(
      '#type' => 'textfield',
    );
    $form['phone'] = array(
      '#type' => 'textfield',
    );
    $form['day'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['month'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['year'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['occupation'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['adrress'] = array(
      '#type' => 'textfield',
    );
    $form['province'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['city'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['district'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['newslatter'] = array(
      '#type' => 'checkbox',
      '#DANGEROUS_SKIP_CHECK'=>true,
    );
    $form['status'] = array(
      '#type' => 'textfield',
    );
    $form['time'] = array(
      '#type' => 'checkboxes',
      '#options' => array(0 => 'Sports',1 => 'Books',2 => 'Music',3 => 'Art',4 => 'Food',5 => 'Movies',6 => 'Technology',7 => 'Coffee',8 => 'Travel',9 => 'Others'),
      '#DANGEROUS_SKIP_CHECK'=>true,
    );
    $form['consume_dolcegusto'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['have_machine'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['register_machine'] = array(
      '#type' => 'select',
      '#validated' => TRUE,
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => array('class' => array('btn','btn-primary')),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // $session = new \Symfony\Component\HttpFoundation\Session\Session();
    // $userData = $session->get('userData');

    $userData = $_SESSION['userData'];
    
    $free_time2 = $form_state->getValue('time');
    foreach ($free_time2 as $key => $value) {
      if(is_numeric($key)) unset($free_time2[$key]);
    }
    $free_time = implode(',', $free_time2);

    $client = \Drupal::service('http_client');
    $result = $client->post('http://dolce-gusto.iris-development.info/apidolcegusto/public/user/update', 
                            ['form_params' => [
                              'id' => $userData->id,
                              'title' => $form_state->getValue('title'),
                              'firstname' => $form_state->getValue('firstname'),
                              'lastname' => $form_state->getValue('lastname'),
                              'email' => $form_state->getValue('email'),
                              'phone' => $form_state->getValue('phone'),
                              'birthday' => $form_state->getValue('year').'-'.$form_state->getValue('month').'-'.$form_state->getValue('day'),
                              'occupation' => $form_state->getValue('occupation'),
                              'address' => $form_state->getValue('adrress'),
                              'province' => $form_state->getValue('province'),
                              'city' => $form_state->getValue('city'),
                              'district' => $form_state->getValue('district'),
                              'newsletter' => $form_state->getValue('newslatter'),
                              'family' => $form_state->getValue('status'),
                              'free_time' => $free_time,
                              'consume_dolcegusto' => $form_state->getValue('consume_dolcegusto'),
                              'have_machine' => $form_state->getValue('have_machine'),
                              'register_machine' => $form_state->getValue('register_machine'),
                            ]]);
    $output = json_decode($result->getBody());

    // $coeg = array('id' => $userData->id,
    //               'title' => $form_state->getValue('title'),
    //               'firstname' => $form_state->getValue('firstname'),
    //               'lastname' => $form_state->getValue('lastname'),
    //               'email' => $form_state->getValue('email'),
    //               'phone' => $form_state->getValue('phone'),
    //               'birthday' => $form_state->getValue('year').'-'.$form_state->getValue('month').'-'.$form_state->getValue('day'),
    //               'occupation' => $form_state->getValue('occupation'),
    //               'adrress' => $form_state->getValue('adrress'),
    //               'province' => $form_state->getValue('province'),
    //               'city' => $form_state->getValue('city'),
    //               'district' => $form_state->getValue('district'),
    //               'newsletter' => $form_state->getValue('newslatter'),
    //               'family' => $form_state->getValue('status'),
    //               'free_time' => $free_time,
    //               'consume_dolcegusto' => $form_state->getValue('consume_dolcegusto'),
    //               'have_machine' => $form_state->getValue('have_machine'),
    //               'register_machine' => $form_state->getValue('register_machine')
    //               );
    // echo '<pre>';
    // print_r($coeg);
    // echo '</pre>';
    // exit;
    // echo '<pre>';
    // $time = implode(',',$form_state->getValue('time'));
    // print_r($form_state->getValue('firstname').' | '.$form_state->getValue('lastname').' | '.$form_state->getValue('email').' | '.$form_state->getValue('phone').' | '.$form_state->getValue('day').' | '.$form_state->getValue('month').' | '.$form_state->getValue('year').' | '.$form_state->getValue('occupation').' | '.$form_state->getValue('adrress').' | '.$form_state->getValue('province').' | '.$form_state->getValue('city').' | '.$form_state->getValue('district').' | '.$form_state->getValue('newslatter').' | '.$form_state->getValue('status').' | '.$time.' | '.$form_state->getValue('consume_dolcegusto').' | '.$form_state->getValue('have_machine').' | '.$form_state->getValue('register_machine'));
    // echo '</pre>';
    // exit;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //$form_state->setRedirect('custompage.mypage');
  
    $url = Url::fromRoute('custompage.profile');

    $form_state->setRedirectUrl($url);

    //drupal_set_message($this->t('Your phone number is @number', array('@number' => $form_state->getValue('phone_number'))));
    // echo 'hahaha';
  }

}