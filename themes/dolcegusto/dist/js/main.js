function show_popup_atc(){
	$('.popupWrapper, .atc_popup').fadeIn();
	$('body, html').css('overflow','hidden');
}
function show_popup_asa(){
	$('.popupWrapper, .asa_popup').fadeIn();
	//$('body, html').css('overflow','hidden');
}
function hide_popup(){
	$('.popupWrapper, .popupBox').fadeOut();
	$('body, html').css('overflow','auto');
}
$(document).ready(function(){
	if($('.home_slider').length>0){
		$('.home_slider').owlCarousel({
			items:1,
			autoplay:true
		});
	}
	if($('.datepicker').length>0){
		$('.datepicker').datepicker();
	}
	if($('.reward_slider').length>0){
		$('.reward_slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			asNavFor: '.reward_thumbs'
		});
		$('.reward_thumbs').slick({
		  slidesToShow: 3,
		  slidesToScroll: 1,
		  asNavFor: '.reward_slider',
		  arrows:true,
		  dots: false,
		  centerMode: true,
		  focusOnSelect: true,
		  autoplaySpeed: 6000
		});
	}
	$('.srd_three ul li a').click(function(){
		var gethref = $(this).attr('href');
		$('.srd_info').hide();
		$('.srd_three ul li a').removeClass('active');
		$(this).addClass('active');
		$(gethref).show();
		return false;
	});
	$('.accordion-title').click(function(){
		if($(this).siblings('.ce-accordion-box').css('display')=="none"){
			$(this).siblings('.ce-accordion-box').slideDown();
			$(this).addClass('opened');
		}
		else{
			$(this).siblings('.ce-accordion-box').slideUp();
			$(this).removeClass('opened');
		}
	});
	$('.menu_address li a').click(function(){
		$('.menu_address li a').removeClass('active');
		$('.addressBox').hide();
		$(this).addClass('active');
		var getID = $(this).attr('href');
		$(getID).show();
		return false;
	});
	$('nav ul li a.toggle_btn').click(function(){
		if($(window).width()<960){
			if($(this).siblings('ul').css('display')=="none"){
				$('nav ul li').removeClass('active');
				$('nav ul li ul').slideUp();
				$(this).parents('li').addClass('active');
				$(this).siblings('ul').slideDown();
			}
			else if($(this).siblings('ul').css('display')=="block"){
				$(this).parents('li').removeClass('active');
				$(this).siblings('ul').slideUp();
			}
		}
	});
	$('.menuBtn').click(function(){
		if($('nav').css('right')=="-1980px"){
			$('nav').addClass('opened');
			$('.headerBottom .overlay').fadeIn();
			$('body, html').css('overflow','hidden');
		}
		else{
			$('nav').removeClass('opened');
			$('.headerBottom .overlay').fadeOut();
			$('body, html').css('overflow','auto');
		}
	});
});