$(document).ready(function(){

	var getFamily = $('#tick').data('family');
	var getFree = $('#tick').data('free');

	//new Clipboard('.btnclip');
	// var getBaseUrl = window.location.protocol + "//" + window.location.host;
	var getBaseUrl = window.location.href;
	// $('.navtop a').removeClass('active');
	// $('.navtop a').each(function(){
	// 	console.log(getBaseUrl);
	// 	if($(this).attr('href') == getBaseUrl){
	// 		$(this).addClass('active');
	// 	}
	// })

	$('.redeem_menu a').removeClass('active');
	$('.redeem_menu a').each(function(){
		console.log(getBaseUrl);
		if($(this).attr('href') == getBaseUrl){
			$(this).addClass('active');
		}
	})

	var dateFormat = "yy/mm/dd",
	  from = $( "#startDate" )
		.datepicker({
		  defaultDate: "+1w",
		  changeMonth: true,
		  numberOfMonths: 1
		})
		.on( "change", function() {
		  to.datepicker( "option", "minDate", getDate( this ) );
		}),
	  to = $( "#endDate" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1
	  })
	  .on( "change", function() {
		from.datepicker( "option", "maxDate", getDate( this ) );
	  });

	function getDate( element ) {
	  var date;
	  try {
		date = $.datepicker.parseDate( "yy-mm-dd", element.value );
	  } catch( error ) {
		date = null;
	  }

	  return date;
	}

	$('.upload_callback').click(function(){
		$('.upload_callback').change(function() {
			var getName = $('input[type=file]').val();
			$('.uploadBox').html(getName);
		});
	});

    // SET INPUT JUST ONE REQUIRED FIELD
    var $inputs = $('input[name=unique_code], input[name=promo_code], input[name=receipt]');
        $inputs.on('input change', function () {
            // Set the required property of the other input to false if this input is not empty.
            $inputs.not(this).prop('required', !$(this).val().length);
        });

    // show alert if url query have message
	if(getUrlParameter('message')){ 
	   if(getUrlParameter('message').length > 3){
		   var msgid = parseInt(getUrlParameter('message'));
		   var message = 'Pesan tidak dikenali, silahkan coba lagi';
		   if(msgid >= 1000){
			   switch(msgid){
					case 1000:
						message = 'Maaf, kode unik yang Anda masukkan tidak ditemukan. Info lebih lanjut hubungi Call Center NDG.';
						break;
					case 1001:
						message = 'Submit point success.';
						break;
					case 1002:
						message = 'Maaf, kode unik yang Anda masukkan tidak valid. Info lebih lanjut hubungi Call Center NDG.';
						break;
					case 1003:
						message = 'Maaf, kode unik yang Anda masukkan sudah digunakan. Info lebih lanjut hubungi Call Center NDG.';
						break;
					case 1004:
						message = 'Maaf, kode unik yang Anda masukkan sudah expired. Info lebih lanjut hubungi Call Center NDG.';
						break;
					case 1005:
						message = 'Maaf, Upload Image gagal';
						break;
			   }
		   }
	   		$('#suctex').html(message);
	   		$('.popupWrapperErr, .overlay1, .errorPopup').fadeIn();
	        //alert(getUrlParameter('message'));
	    }
	}
    function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }

	$('#selectProvince').change(function(){
		var texthtml = '';
		var str = $(this).val();
		str = str.replace(/\s+/g, '');
		$('#selectCity > .class-'+str).each(function(){
			texthtml += $(this).html();
		});
		$('#cityAppend').html(texthtml);
	});

	if(getFree){
		var getArr = getFree.split(",");
		$('.hobbiesBox > input[type="checkbox"]').each(function(){
			// console.log(getArr);
			if(getArr.indexOf($(this).val()) >= 0){
				$(this).attr("checked", "checked");
			}
		})
	}

	$('.statusBox > input[type="radio"]').each(function(){
		// console.log(getArr);
		if($(this).val() == getFamily){
			$(this).attr("checked", "checked");
		}
	});

	$('.total_class').change(function(){
		// alert($(this).val());
		if($(this).val() > 0){
			$(this).closest('.row_redem_data').find(' > .product_id').addClass('setval');
		}else{
			$(this).closest('.row_redem_data').find(' > .product_id').removeClass('setval');
		}
	});
	$('#btnAddCart').on('click', function(){
		
		var arrData = {};
		var arrBig = [];
		var checkData = 'true';
		var getBaseUrl = $('.rewardInfoBox').data('baseurl');

		var getLogged = $('.popupWrapper').data('logged');
		$(this).addClass('loading');
        $(this).attr('disabled', 'disabled');
		if(getLogged){
			$('.row_redem_data').each(function(){
				arrData = {};
				// if($(this).find(' > .setval').length > 0){
					$(this).find('.input_class').each(function(){

						arrData[$(this).attr('name')] = $(this).val();
						if(!$(this).val()){
							checkData = 'false';
						}
					});

					console.log(arrData);
					if(checkData != 'false'){
						arrBig.push(arrData);
					}
				// }
			});

			if(arrBig.length > 0){
				var data_cart = JSON.stringify(arrBig);
				 //console.log(arrBig);
				//console.log(data_cart);
				$.post( getBaseUrl+'addtocart', { data_cart: data_cart })
					.done(function( data ) {
						var t = JSON.parse(data);
						if(t['status'] == "0"){
							//alert('Maaf, Point Anda Tidak Cukup');
							$('#errorRedeemMessage').html(t['message']);
							$('.popupWrapperErr, .overlay1, .errorPopup').fadeIn();
							$('#btnAddCart').removeClass('loading');
                            $('#btnAddCart').removeAttr('disabled');
						}else{
							if($('#cartCount').html()){
								var total_cart = parseInt($('#cartCount').html()) + t['total'];
								$('#cartCount').html(total_cart);
							}else{
								$('.cartBtn').html('Cart <span id="cartCount">'+t['total']+'</span>');
							}

							$('.popupWrapperSuc, .overlay2, .rewardPopup').fadeIn();
							$('#btnAddCart').removeClass('loading');
                            $('#btnAddCart').removeAttr('disabled');
						}
				});
			}else{
				return;
				// $('.popupWrapper2, .overlay2, .rewardPopup2').fadeIn();
				//fix
			}

			//console.log(arrData);
		}else{
			$('.popupWrapperSuc, .overlay2, .rewardPopup').fadeIn();
			$('#btnAddCart').removeClass('loading');
            $('#btnAddCart').removeAttr('disabled');
		}

	});

	$('input[name="newsletter"]').on('click', function(){
		if($(this).val() == '0'){
			$('input[name="newsletter_type[]"]').removeAttr('required');
			$('input[name="newsletter_type[]"]').attr('disabled','disabled');
		}else{
			$('input[name="newsletter_type[]"]').attr('required', 'required');
			$('input[name="newsletter_type[]"]').removeAttr('disabled');
		}
	});

	$('input[name="newsletter_type[]"]').on('click', function(){
		var check = 0;
		$('input[name="newsletter_type[]"]').each(function(){
			if($(this).is(":checked")){
				check = 1
			}
		});

		if(check){
			$('input[name="newsletter_type[]"]').removeAttr('required');
		}else{
			$('input[name="newsletter_type[]"]').attr('required', 'required');
		}
	})

	$('.btnAddCartSingle').on('click', function(){
		
		var arrData = {};
		var arrBig = [];
		var checkData = 'true';
		var getBaseUrl = $('#base').data('baseurl');
		var getLogged = $('#base').data('logged');
		$(this).addClass('loading');
        $(this).attr('disabled', 'disabled');
		if(getLogged){
			//$('.row_redem_data').each(function(){
				arrData = {};
				// if($(this).find(' > .setval').length > 0){
					// $(this).find('.input_class').each(function(){

					// 	arrData[$(this).attr('name')] = $(this).val();
					// 	if(!$(this).val()){
					// 		checkData = 'false';
					// 	}
					// });
					arrData['product_id'] = $(this).data('product_id');
					arrData['type'] = 1;
					arrData['jumlah'] = 1;
					arrBig.push(arrData);
					//console.log(arrData);
					// if(checkData != 'false'){
					// 	arrBig.push(arrData);
					// }
				// }
			//});

			if(arrBig.length > 0){
				var data_cart = JSON.stringify(arrBig);
				// console.log(arrBig);
				//console.log(data_cart);
				$.post( getBaseUrl+'addtocart', { data_cart: data_cart })
					.done(function( data ) {
						var t = JSON.parse(data);
						//console.log(data);
						if(t['status'] == "0"){
							//alert('Maaf, Point Anda Tidak Cukup');
							$('#errorRedeemMessage').html(t['message']);
							$('.popupWrapperErr, .overlay1, .errorPopup').fadeIn();
							$('.btnAddCartSingle').removeClass('loading');
                            $('.btnAddCartSingle').removeAttr('disabled');
							
						}else{
							//console.log($(this));
							if($('#cartCount').html()){
								var total_cart = parseInt($('#cartCount').html()) + 1;
								$('#cartCount').html(total_cart);
							}else{
								$('.cartBtn').html('Cart <span id="cartCount"> 1 </span>');
							}
							$('.popupWrapperSuc, .overlay2, .rewardPopup').fadeIn();
							$('.btnAddCartSingle').removeClass('loading');
                            $('.btnAddCartSingle').removeAttr('disabled');
							
						}
						
				});
			}else{
				return;
				// $('.popupWrapper2, .overlay2, .rewardPopup2').fadeIn();
				//fix
			}

			//console.log(arrData);
		}else{
			$('.popupWrapperSuc, .overlay2, .rewardPopup').fadeIn();
			$('.btnAddCartSingle').removeClass('loading');
            $('.btnAddCartSingle').removeAttr('disabled');
			
		}

	});

	$('.deleteBtn').click(function(){
		var getBaseUrl = $('.summaryBox').data('baseurl');
		var cartID = $(this).data('cartid');
		var thisElem = $(this);


		// //test data
		// var totalPoint = 0;
		// totalPoint = parseInt($('.totalRedeemBox').html()) - parseInt(thisElem.data('point'));
		// console.log(parseInt($('.totalRedeemBox').html()));
		// console.log(parseInt(thisElem.data('point')));
		// console.log(totalPoint);
		// // $('.needBox').each(function(){
		// // 	totalPoint += parseInt($(this).data('point'));
		// // });
		// $('.totalRedeemBox').html(totalPoint);
		// thisElem.parents('.summaryRow').hide().html('');
		// //end test data


		$.post( getBaseUrl+'removecart', { cartID : cartID })
			.done(function( data ) {
				// console.log(data);
				var totalPoint = 0;
				totalPoint = parseInt($('.totalRedeemBox').html()) - parseInt(thisElem.data('point'));
				// console.log(parseInt($('.totalRedeemBox').html()));
				// console.log(parseInt(thisElem.data('point')));
				// console.log(totalPoint);
				// $('.needBox').each(function(){
				// 	totalPoint += parseInt($(this).data('point'));
				// });
				var total_cart = parseInt($('#cartCount').html()) - parseInt(thisElem.data('quantity'));
				$('#cartCount').html(total_cart);
				$('.totalRedeemBox').html(totalPoint);
				thisElem.parents('.summaryRow').hide().html('');
		});

		return false;
	});



	$('.addAddressBtn').click(function(){
		$(this).hide();
		var getProvince = $('#selectProvince').html();
		$('.addressWrapper').append('<div class="addressBox">\
                                        <input type="radio" name="address" checked value="addnew">\
                                        <label><span></span></label>\
                                        <div class="address">\
                                            <span class="address_title">Alamat 2</span>\
                                            <div class="fullBox register">\
                                                <div class="one_columns">\
                                                    <span class="info">Alamat</span>\
                                                    <textarea name="addresstext"></textarea>\
                                                </div>\
                                            </div>\
                                            <div class="fullBox register">\
                                                <div class="one_columns">\
                                                    <span class="info">Patokan</span>\
                                                    <textarea name="patokan"></textarea>\
                                                </div>\
                                            </div>\
                                            <div class="fullBox register">\
                                                <div class="three_columns">\
                                                    <span class="info">Provinsi</span>\
                                                    <select id="selectProvince" name="province" data-tags="true" data-placeholder="-Select Provinsi-">\
                                                        '+getProvince+'\
                                                    </select>\
                                                </div>\
                                                <div class="three_columns">\
                                                    <span class="info">Kota</span>\
                                                    <select id="cityAppend" name="city" data-tags="true" data-placeholder="-Select Kota-">\
                                                        <option></option>\
                                                    </select>\
                                                </div>\
                                            </div>\
                                            <div class="fullBox register">\
                                                <div class="three_columns">\
                                                    <span class="info">Kecamatan</span>\
                                                    <select id="appenddistrict" name="district" data-tags="true" data-placeholder="-Select Kecamatan-">\
                                                    	<option></option>\
                                                    </select>\
                                                </div>\
                                            </div>\
                                            <div class="fullBox register">\
                                                <div class="three_columns">\
                                                    <span class="info">Kode Pos</span>\
                                                    <input name="postal_code" type="text">\
                                                </div>\
                                                <div class="three_columns">\
                                                    <span class="info">No. Telepon</span>\
                                                    <input name="phone" type="text">\
                                                </div>\
                                            </div>\
                                            <div class="fullBox register">\
                                                <input name="saveaddress" type="checkbox"> Simpan alamat baru\
                                            </div>\
                                        </div>\
                                    </div>');
		$('select').select2({
			minimumResultsForSearch: Infinity
		});

		$('#selectProvince').change(function(){
			var texthtml = '';
			var str = $(this).val();
			str = str.replace(/\s+/g, '');
			$('#selectCity > .class-'+str).each(function(){
				texthtml += $(this).html();
			});
			$('#cityAppend').html(texthtml);
		});
		$('#cityAppend').change(function(){
			var getBaseUrl = $('#formEditProfile').data('baseurl');
			var getThisVal = $(this).val();
			$.post( getBaseUrl+'selectdistrict', { idcity : getThisVal })
				.done(function( data ) {
					// console.log(getThisVal+' - '+data);
					var html = '';
					$.each(JSON.parse(data), function(key, item){
						html += '<option>'+item.name+'</option>';
					});
					// console.log(html);
					$('#appenddistrict').html(html);
			});
		});

		return false;
	});

	$('#cityAppend').change(function(){
		var getBaseUrl = $('#formEditProfile').data('baseurl');
		var getThisVal = $(this).val();
		//alert(getBaseUrl);
		$.post( getBaseUrl+'selectdistrict', { idcity : getThisVal })
			.done(function( data ) {
				// console.log(getThisVal+' - '+data);
				var html = '';
				$.each(JSON.parse(data), function(key, item){
					html += '<option>'+item.name+'</option>';
				});
				// console.log(html);
				$('#appenddistrict').html(html);
		});
	});

	$('#channelAppend').change(function(){
		var getBaseUrl = $('#formsubmitpoint').data('baseurl');
		var getThisVal = $(this).val();
		//alert(getThisVal);
		$.post( getBaseUrl+'selectstore', { idcity : getThisVal })
			.done(function( data ) {
				//console.log(getThisVal+' - '+data);
				var html = '';
				$.each(JSON.parse(data), function(key, item){
					html += '<option value="'+item.name+'">'+item.name+'</option>';
				});
				// console.log(html);
				$('#appendstore').html(html);
		});
	});

	$('.plusBtn1').on('click', function(){
        var count = $(this).closest('.quantityBox').find('input[name="jumlah"]').val();
        count++;
        $(this).closest('.quantityBox').find('input[name="jumlah"]').val(count);
    });
    $('.minusBtn1').on('click', function(){
        var count = $(this).closest('.quantityBox').find('input[name="jumlah"]').val();
      if (count >= 1) {
        count--;
        $(this).closest('.quantityBox').find('input[name="jumlah"]').val(count);
      }  
    });


	// var html = 'print_r(';
	// $('[name]').each(function(){
	// 	html += "$form_state->getValue('"+$(this).attr('name')+"').' | '.";
	// 	// html += "'"+$(this).attr('name')+"' => $form_state->getValue('"+$(this).attr('name')+"'),<br>";
	// 	// html += "$form['"+$(this).attr('name')+"'] = array();<br>";
	// });
	// html += ')';
	// $('#hah').html(html);
	$("#birthday").datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                yearRange: "-100:+0",
                dateFormat: 'yy-mm-dd'
            });
});

function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
              $('.imgPreview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
          }
        }
        $(".photoBtn").change(function() {
          readURL(this);
          $('.imgPreview').css('opacity',1);
    });

$('#upcontact').each(function() {
			$(this).change(function(){
			    var filename = $(this).val();
			    var lastIndex = filename.lastIndexOf("\\");
			    if (lastIndex >= 0) {
			        filename = filename.substring(lastIndex + 1);
			    }
		   		$('.fileTxt').css('margin-bottom',10).html(filename);
		    })
	});

$('#imgupload').change(function(){

        var uploadurl = $(this).closest('form').attr('action');
        var data = new FormData();
        data.append('image', $(this)[0].files[0]);

        $.ajax({
            url: uploadurl,
            data: data,
            cache: false,
            type: 'POST',
            processData: false,
            contentType: false,
            success: function(data){
                $('.upload > img').attr('src', data);
            }
        });
    });

var clipboard = new ClipboardJS('.btn');
