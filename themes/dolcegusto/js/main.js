function show_rewardpopup(){
	$('.popupWrapper, .overlay, .rewardPopup').fadeIn();
}
function show_receiptpopup(){
	$('.popupWrapper, .overlay, .receiptPopup').fadeIn();
}
function hide_popup(){
	$('.popupWrapper, .overlay, .popupBox').fadeOut();
}
$(document).ready(function(){
	if($('.bannerBox').length>0){
		$('.bannerBox').owlCarousel({
			items:1,
			dots:true,
			dotsEach:true,
			autoplay: true,
			autoplayHoverPause: true,
			loop: true
		});
	}
	if($('.blogBanner').length>0){
		$('.blogSlider').owlCarousel({
			items:1,
			dots:true,
			autoplay:true,
			autoplayTimeout:3000,
			loop:true,
			dotsEach:true
		});
	}
	if($('select').length>0){
		$('select').select2({
			minimumResultsForSearch: Infinity
		});
	}
	$('.statusBox input[type="radio"]').click(function(){
		$(this).attr('checked','checked');
		$(this).parents('.statusBox').siblings().find('input[type="radio"]').removeAttr('checked');
	});
	$('.hobbiesBox input[type="checkbox"]').click(function(){
		if($(this).prop('checked')==false){
			$(this).removeAttr('checked');
		}
		else{
			$(this).attr('checked','checked');
		}
	});
	$('.machineListBox input[type="radio"').click(function(){
		$(this).attr('checked','checked');
		$(this).parents('.machineListBox').siblings().find('input[type="radio"]').removeAttr('checked');
	});
	var personal_data = true
	$('.personalData .titleBar').click(function(){
		if(personal_data==true){
			$(this).addClass('closed');
			personal_data = false;
		}
		else{
			$(this).removeClass('closed');
			personal_data = true;
		}
		$(this).siblings('.registerBox').slideToggle();
	});
	var about_me = true
	$('.aboutMe .titleBar').click(function(){
		if(about_me==true){
			$(this).addClass('closed');
			about_me = false;
		}
		else{
			$(this).removeClass('closed');
			about_me = true;
		}
		$(this).siblings('.registerBox').slideToggle();
	});
	$('.previewBox span').click(function(){
		$('.previewBox img').attr('src', '');
		$('.previewBox').hide();
	})
	$('#upload_unique').click(function(){
		$('#upload_unique').change(function() {
			var getName = $('input[type=file]').val();
			$('.pathBox').html(getName);
		});
	});
	$('.deleteBtn').click(function(){
		$(this).parents('.summaryRow').hide().html('');
		return false;
	});
	var menu_mobile = true;
	$('.menuMobile').click(function(){
		if(menu_mobile == true){
			$('.navBox').addClass('orangeBg');
			$('.bottom_menu').show();
			$('body, html').css('overflow','hidden');
			menu_mobile = false;
		}
		else{
			$('.navBox').removeClass('orangeBg');
			$('.bottom_menu').hide();
			$('body, html').css('overflow','visible');
			menu_mobile = true;
		}
	});
	var result_menu = true
	$('.resultMenu').click(function(){
		if(result_menu==true){
			$(this).addClass('open');
			result_menu = false;
		}
		else{
			$(this).removeClass('open');
			result_menu = true;
		}
		$('.sideMenuWrapper ul').slideToggle();
	});
});
$(window).bind('load resize', function() {
	var footerHeight = $('footer').innerHeight();
	$('section:last-of-type').css('padding-bottom', footerHeight);
	if($(window).width()>959){
		$('.bottom_menu').show();
	}
	else{
		$('.bottom_menu').hide();
	}
});