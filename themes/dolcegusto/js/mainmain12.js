$(window).load(function(){
	//when onload
	if($('.fullBox.register select[name="have_machine"]').val() == 'Yes'){
		$('.fullBox.register select[name="register_machine"]').show();	
	}else{
		$('.fullBox.register select[name="register_machine"]').hide();
	}
});

$(document).ready(function(){

	var getFamily = $('#tick').data('family');
	var getFree = $('#tick').data('free');
	
	new Clipboard('.btnclip');

	var dateFormat = "yy/mm/dd",
	  from = $( "#startDate" )
		.datepicker({
		  defaultDate: "+1w",
		  changeMonth: true,
		  numberOfMonths: 1
		})
		.on( "change", function() {
		  to.datepicker( "option", "minDate", getDate( this ) );
		}),
	  to = $( "#endDate" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1
	  })
	  .on( "change", function() {
		from.datepicker( "option", "maxDate", getDate( this ) );
	  });

	function getDate( element ) {
	  var date;
	  try {
		date = $.datepicker.parseDate( "yy-mm-dd", element.value );
	  } catch( error ) {
		date = null;
	  }
 
	  return date;
	}
	

$('.fullBox.register select[name="have_machine"]').on('change', function(){
	if($(this).val() == 'Yes'){
		$(this).closest('.fullBox.register').next().show();
	}else{
		$(this).closest('.fullBox.register').next().hide();
	}
});

	$('.upload_callback').click(function(){
		$('.upload_callback').change(function() {
			var getName = $('input[type=file]').val();
			$('.uploadBox').html(getName);
		});
	});

	$('#selectProvince').change(function(){
		var texthtml = '';
		var str = $(this).val();
		str = str.replace(/\s+/g, '');
		$('#selectCity > .class-'+str).each(function(){
			texthtml += $(this).html();
		});
		$('#cityAppend').html(texthtml);
	});

	if(getFree){
		var getArr = getFree.split(",");
		$('.hobbiesBox > input[type="checkbox"]').each(function(){
			// console.log(getArr);
			if(getArr.indexOf($(this).val()) >= 0){
				$(this).attr("checked", "checked");
			}
		})
	}

	$('.statusBox > input[type="radio"]').each(function(){
		// console.log(getArr);
		if($(this).val() == getFamily){
			$(this).attr("checked", "checked");
		}
	});

	$('.total_class').change(function(){
		// alert($(this).val());
		if($(this).val() > 0){
			$(this).closest('.row_redem_data').find(' > .product_id').addClass('setval');
		}else{
			$(this).closest('.row_redem_data').find(' > .product_id').removeClass('setval');			
		}
	});
	$('#btnAddCart').on('click', function(){
		var arrData = {};
		var arrBig = [];
		var checkData = 'true';
		var getBaseUrl = $('.rewardInfoBox').data('baseurl');

		var getLogged = $('.popupWrapper').data('logged');
		
		if(getLogged){
			$('.row_redem_data').each(function(){
				arrData = {};
				if($(this).find(' > .setval').length > 0){
					$(this).find('.input_class').each(function(){
						
						arrData[$(this).attr('name')] = $(this).val();
						if(!$(this).val()){
							checkData = 'false';
						}
					});
					// console.log(checkData);
					if(checkData != 'false'){
						arrBig.push(arrData);
					}
				}
			});

			if(arrBig.length > 0){
				var data_cart = JSON.stringify(arrBig);
				// console.log(arrBig);
				// console.log(data_cart);
				$.post( getBaseUrl+'/addtocart', { data_cart: data_cart })
					.done(function( data ) {
						// console.log(data);
						$('.popupWrapper, .overlay, .rewardPopup').fadeIn();
				});
			}else{
				return;
				// $('.popupWrapper2, .overlay2, .rewardPopup2').fadeIn();
				//fix
			}
		}else{
			$('.popupWrapper, .overlay, .rewardPopup').fadeIn();
		}

	});

	$('.deleteBtn').click(function(){
		var getBaseUrl = $('.summaryBox').data('baseurl');
		var cartID = $(this).data('cartid');
		var thisElem = $(this);


		// //test data
		// var totalPoint = 0;	
		// totalPoint = parseInt($('.totalRedeemBox').html()) - parseInt(thisElem.data('point'));
		// console.log(parseInt($('.totalRedeemBox').html()));
		// console.log(parseInt(thisElem.data('point')));
		// console.log(totalPoint);
		// // $('.needBox').each(function(){
		// // 	totalPoint += parseInt($(this).data('point'));
		// // });
		// $('.totalRedeemBox').html(totalPoint);
		// thisElem.parents('.summaryRow').hide().html('');
		// //end test data


		$.post( getBaseUrl+'/removecart', { cartID : cartID })
			.done(function( data ) {
				// console.log(data);
				var totalPoint = 0;	
				totalPoint = parseInt($('.totalRedeemBox').html()) - parseInt(thisElem.data('point'));
				// console.log(parseInt($('.totalRedeemBox').html()));
				// console.log(parseInt(thisElem.data('point')));
				// console.log(totalPoint);
				// $('.needBox').each(function(){
				// 	totalPoint += parseInt($(this).data('point'));
				// });
				$('.totalRedeemBox').html(totalPoint);
				thisElem.parents('.summaryRow').hide().html('');
		});
		
		return false;
	});



	$('.addAddressBtn').click(function(){
		$(this).hide();
		var getProvince = $('#selectProvince').html();
		$('.addressWrapper').append('<div class="addressBox">\
                                        <input type="radio" name="address" checked value="addnew">\
                                        <label><span></span></label>\
                                        <div class="address">\
                                            <span class="address_title">Alamat 2</span>\
                                            <div class="fullBox register">\
                                                <div class="one_columns">\
                                                    <span class="info">Alamat</span>\
                                                    <textarea name="addresstext"></textarea>\
                                                </div>\
                                            </div>\
                                            <div class="fullBox register">\
                                                <div class="one_columns">\
                                                    <span class="info">Patokan</span>\
                                                    <textarea name="patokan"></textarea>\
                                                </div>\
                                            </div>\
                                            <div class="fullBox register">\
                                                <div class="three_columns">\
                                                    <span class="info">Provinsi</span>\
                                                    <select id="selectProvince" name="province" data-tags="true" data-placeholder="-Select Provinsi-">\
                                                        '+getProvince+'\
                                                    </select>\
                                                </div>\
                                                <div class="three_columns">\
                                                    <span class="info">Kota</span>\
                                                    <select id="cityAppend" name="city" data-tags="true" data-placeholder="-Select Kota-">\
                                                        <option></option>\
                                                    </select>\
                                                </div>\
                                            </div>\
                                            <div class="fullBox register">\
                                                <div class="three_columns">\
                                                    <span class="info">Kecamatan</span>\
                                                    <select id="appenddistrict" name="district" data-tags="true" data-placeholder="-Select Kecamatan-">\
                                                    	<option></option>\
                                                    </select>\
                                                </div>\
                                            </div>\
                                            <div class="fullBox register">\
                                                <div class="three_columns">\
                                                    <span class="info">Kode Pos</span>\
                                                    <input name="postal_code" type="text">\
                                                </div>\
                                                <div class="three_columns">\
                                                    <span class="info">No. Telepon</span>\
                                                    <input name="phone" type="text">\
                                                </div>\
                                            </div>\
                                            <div class="fullBox register">\
                                                <input name="saveaddress" type="checkbox"> Simpan alamat baru\
                                            </div>\
                                        </div>\
                                    </div>');
		$('select').select2({
			minimumResultsForSearch: Infinity
		});
		
		$('#selectProvince').change(function(){
			var texthtml = '';
			var str = $(this).val();
			str = str.replace(/\s+/g, '');
			$('#selectCity > .class-'+str).each(function(){
				texthtml += $(this).html();
			});
			$('#cityAppend').html(texthtml);
		});
		$('#cityAppend').change(function(){
			var getBaseUrl = $('#formEditProfile').data('baseurl');
			alert(getBaseUrl);
			var getThisVal = $(this).val();
			$.post( getBaseUrl+'/selectdistrict', { idcity : getThisVal })
				.done(function( data ) {
					// console.log(getThisVal+' - '+data);
					var html = '';
					$.each(JSON.parse(data), function(key, item){
						html += '<option>'+item.name+'</option>';
					});
					// console.log(html);
					$('#appenddistrict').html(html);
			});
		});

		return false;
	});

	$('#cityAppend').change(function(){
		//var getBaseUrls = $('#formEditProfile').attr('data-baseurl');
		//alert(getBaseUrls);
		var getBaseUrls = 'https://dolce-gusto.sahabatnestlerewards.co.id';
		var getThisVal = $(this).val();
		$.post( getBaseUrls+'/selectdistrict', { idcity : getThisVal })
			.done(function( data ) {
				// console.log(getThisVal+' - '+data);
				var html = '';
				$.each(JSON.parse(data), function(key, item){
					html += '<option>'+item.name+'</option>';
				});
				// console.log(html);
				$('#appenddistrict').html(html);
		});
	});


	
	// var html = 'print_r(';
	// $('[name]').each(function(){
	// 	html += "$form_state->getValue('"+$(this).attr('name')+"').' | '.";
	// 	// html += "'"+$(this).attr('name')+"' => $form_state->getValue('"+$(this).attr('name')+"'),<br>";
	// 	// html += "$form['"+$(this).attr('name')+"'] = array();<br>";
	// });
	// html += ')';
	// $('#hah').html(html);
});
