$(document).ready(function(){
	//get base url
	// var getBaseUrl = $('link#baseUrl').attr('href');
	var csrfToken = $('div#CSRF').data('token');
	var csrfHash = $('div#CSRF').data('hash');

	if($('#help').data('success') != ''){
		alert($('#help').data('success'));
	}
	
	if($('#help').data('active-user') != ''){
		alert($('#help').data('active-user'));
	}

	function statusChangeCallback(response) {
	    console.log('statusChangeCallback');
	    console.log(response);
	    // The response object is returned with a status field that lets the
	    // app know the current login status of the person.
	    // Full docs on the response object can be found in the documentation
	    // for FB.getLoginStatus().
	    FB.login(function(response) {
	      if (response.status === 'connected') {
	        // Logged into your app and Facebook.
	        getUserData();
	      } else if (response.status === 'not_authorized') {
	        // The person is logged into Facebook, but not your app.
	        document.getElementById('status').innerHTML = 'Please log ' +
	          'into this app.';
	      } else {
	        // The person is not logged into Facebook, so we're not sure if
	        // they are logged into this app or not.
	        document.getElementById('status').innerHTML = 'Please log ' +
	          'into Facebook.';
	      }
	    }, {scope: 'public_profile,email'});
	  }

	  // This function is called when someone finishes with the Login
	  // Button.  See the onlogin handler attached to it in the sample
	  // code below.
	  function checkLoginState() {
	    FB.getLoginStatus(function(response) {
	      statusChangeCallback(response);
	    });
	  }

	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '1598918590419443',
	      cookie     : true,  // enable cookies to allow the server to access 
	                          // the session
	      xfbml      : true,  // parse social plugins on this page
	      version    : 'v2.2' // use version 2.2
	    });

	    // Now that we've initialized the JavaScript SDK, we call 
	    // FB.getLoginStatus().  This function gets the state of the
	    // person visiting this page and can return one of three states to
	    // the callback you provide.  They can be:
	    //
	    // 1. Logged into your app ('connected')
	    // 2. Logged into Facebook, but not your app ('not_authorized')
	    // 3. Not logged into Facebook and can't tell if they are logged into
	    //    your app or not.
	    //
	    // These three cases are handled in the callback function.

	    // FB.getLoginStatus(function(response) {
	    //   statusChangeCallback(response);
	    // });

	  };

	  // Load the SDK asynchronously
	  (function(d, s, id) {
	    var js, fjs = d.getElementsByTagName(s)[0];
	    if (d.getElementById(id)) return;
	    js = d.createElement(s); js.id = id;
	    js.src = "//connect.facebook.net/en_US/sdk.js";
	    fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));

	  // Here we run a very simple test of the Graph API after login is
	  // successful.  See statusChangeCallback() for when this call is made.
	  function getUserData() {
	    FB.api('/me?fields=id,name,picture,email,gender,first_name,last_name,link,timezone,locale,age_range', function(response) {
	      $.post( getBaseUrl+'user/login_fb', { franknco_token : csrfHash, data: JSON.stringify(response) })
	        .done(function( data ) {
	        	if(currentUrl == getBaseUrl+'user/register'){
		        	window.location.replace(getBaseUrl+'home');
			    }else{
			    	location.reload();
			    }
	        });
	    });
	  }
	  // $('#share').on('click', function(){
	  //   alert('tolol');
	  //   FB.ui({
	  //     method: 'share',
	  //     href: 'https://developers.facebook.com/docs/',
	  //   }, function(response){});
	  // });
	 $('.btn-connectfb').on('click', function(){
	 	checkLoginState();
	 });


	 $('.btn-addToWishlist').on('click', function(){
	 	var idp = $(this).data('idp');
	 	var tagid = $(this).data('tagid');
	 	$.get( getBaseUrl+'user/add_wishlist', 'product_id='+idp+'&tag_id='+tagid+'&franknco_token='+csrfHash)
        .done(function( data ) {
        	alert(data);
        	if(data =='Berhasil menambahkan ke wishlist'){
	        	var cWishlist = parseInt($('a#btn-diamond span').text());
	          	$('a#btn-diamond span').text(cWishlist+1);
	          }
          	// $( location ).attr('href', redirect_uri);
          	// console.log(data);
    	});
	});

	 $('.remove-wishlist').on('click', function(){
	 	var thisElement = $(this);
	 	var idp = $(this).data('idp');
	 	$.get( getBaseUrl+'user/remove_wishlist', 'product_id='+idp+'&franknco_token='+csrfHash)
        .done(function( data ) {
        	alert(data);
        	if(data =='Berhasil menghapus wishlist'){
	        	var cWishlist = parseInt($('a#btn-diamond span').text());
	          	$('a#btn-diamond span').text(cWishlist-1);

	          	thisElement.closest('.dream-item').remove();
	          }
          	// $( location ).attr('href', redirect_uri);
          	// console.log(data);
    	});
	 });

	var password = $("#password")
	  , confirm_password = $("#confirm_password");

	function validatePassword(){
	  if(password.value != confirm_password.value) {
	    confirm_password.setCustomValidity("Passwords Don't Match");
	  } else {
	    confirm_password.setCustomValidity('');
	  }
	}

	password.onchange = validatePassword;
	confirm_password.onkeyup = validatePassword;

	$('input, select').removeAttr('required');

});