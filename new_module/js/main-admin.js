$(document).ready(function(){
	$('.redactor').redactor({
        plugins:['fullscreen','source']
    });

    function call_redactor(){
    	$('.redactor').redactor({
	        plugins:['fullscreen','source']
	    });
    }

    $('#element-type').on('change', function(){
    	var html = '';
    	switch($(this).val()){
    		case '1' : html = '<div class="form-group">\
		                          <label for="cname">Text <span class="required">*</span></label>\
		                          <textarea name="text" class="form-control redactor" required></textarea>\
		                      </div>';
                      break;
            case '2' : html = '<div class="form-group">\
		                          <label for="cname">Image <span class="required">*</span></label>\
		                          <input type="file" name="image" required>\
		                      </div>';
                      break;
            case '3' : html = '<div class="form-group">\
		                          <label for="cname">Image Left <span class="required">*</span></label>\
		                          <input type="file" name="image_1" required>\
		                      </div>\
		                      <div class="form-group">\
		                          <label for="cname">Image Right <span class="required">*</span></label>\
		                          <input type="file" name="image_2" required>\
		                      </div>';
                      break;
            case '4' : html = '<div class="form-group">\
			                    <label for="cname">Video Link</label>\
			                    <div class="input-group">\
			                            <span class="input-group-addon">https://www.youtube.com/embed/</span>\
			                            <input class="form-control" name="video" placeholder="ex: SMriNOw_e24" required type="text">\
			                        </div>\
				                </div>';
				                break;
            case '5' : html = '<div class="form-group">\
		                          <label for="cname">Image Align <span class="required">*</span></label><br>\
		                          <div class="radio"><label><input type="radio" name="align" value="L" checked> Left</label></div>\
		                          <div class="radio"><label><input type="radio" name="align" value="R"> Right</label></div>\
		                      </div>\
		                      <div class="form-group">\
		                          <label for="cname">Image <span class="required">*</span></label>\
		                          <input type="file" name="image" required>\
		                      </div>\
		                      <div class="form-group">\
		                          <label for="cname">Text <span class="required">*</span></label>\
		                          <textarea name="text" class="form-control redactor" required></textarea>\
		                      </div>';
                      break;
    	}
    	$('#content-type').html('');
    	$('#content-type').append(html);
    	call_redactor();
    });


    $('.linkType').each(function(){
	    change_linktype($(this), $(this).data('link'));
	});

    $('.linkType').on('change', function(){
    	change_linktype($(this), $(this).data('link'));
    });


    function change_linktype(thisElement, linkValue){
    	var html = '';
    	var getBaseUrl = thisElement.data('baseurl');
    	switch(thisElement.val()){
    		case '0' : html = '<div class="form-group">\
		                          <input type="text" name="link" class="form-control" readonly>\
		                      </div>';
                      break;
            case '1' : html = '<div class="form-group">\
							    <div class="input-group">\
							      <div class="input-group-addon">'+getBaseUrl+'</div>\
							      <input type="text" name="link" class="form-control" value="'+linkValue+'">\
							    </div>\
							  </div>';
                      break;
            case '2' : html = '<div class="form-group">\
		                          <input type="text" name="link" class="form-control" value="'+linkValue+'">\
		                      </div>';
                      break;
    	}
    	thisElement.closest('.blok-link').find('.contentLink').html('');
    	thisElement.closest('.blok-link').find('.contentLink').append(html);
    }


 //    change_link_template($('.check_template').val());
 //    $('.check_template').on('click', function(){
 //    	change_link_template($(this).val());
	// });

	// function change_link_template(thisVal){
	//     var getTemplateUrl = $('#highligh_template_url').val();	    
	// 	$('#change_link_template').attr('href', getTemplateUrl+'/'+thisVal);
	// }

	$('.sortable-product').sortable({
        
        items: "tr:not(.nosortable)",
        placeholder: "ui-state-highlight",
        update: function( event, ui ) {
            
        },
        stop: function( event, ui ) {
            var sortOrder = 1;
            
            $('#product-order > tr').each(function(){
                thiselem = $(this);
                oldprecedence = thiselem.data('precedence');
//                newprecedence = thiselem.data('newprecedence');
                maincontent_id = thiselem.data('id');
                
                $.ajax({
                    type: 'GET',
                    url: base_url+'fcadmin/product/update_precedence',
                    data: 'maincontent_id='+maincontent_id+
                    		'&oldprecedence='+oldprecedence+
                            '&newprecedence='+sortOrder,
                    cache : false,
                    success: function(msg){
                        //alert(msg);
                        console.log(msg);
                    },
                    error: function(msg){
                        console.log(msg);
                    }
                });
                thiselem.data('precedence', sortOrder);
                thiselem.find('td.prece').text(sortOrder);
                
                sortOrder++;
            });
            
        }
    });
});